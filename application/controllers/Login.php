<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('auth');
	}

	public function index()
	{
		$data['title'] = 'LHP Login';
		$this->load->view('tamplateAuth/authHeader', $data);
		$this->load->view('auth/login');
		$this->load->view('tamplateAuth/authFooter');
		
	}

	public function proses()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if($this->auth->login_user($username,$password))
		{
			redirect('http://localhost/lhp_wide_strip/home');
		}
		else
		{
			$this->session->set_flashdata('error','Username & Password salah');
			redirect('http://localhost/lhp_wide_strip/login');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('is_login');
		redirect('http://localhost/lhp_wide_strip/login');
	}

	

}
