<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lhp_wide_strip extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_lhp_wide_strip');
		$this->load->library('session');
	}

	public function index()
	{
		$data['data_operator']=$this->m_lhp_wide_strip->getDataOperator();
		$data['data_downtime']=$this->m_lhp_wide_strip->getDataDowntime();
		$data['username'] = $this->session->userdata('username');
		$data['title'] = 'Checksheet Wide Strip';
		$this->load->view('tamplate/authHeader', $data);
		$this->load->view('v_lhp_wide_strip', $data);
		$this->load->view('tamplate/authFooter');
	}

	public function edit($tanggal='',$shift='') {
        // Panggil model untuk mengambil data
        $this->load->model('m_lhp_wide_strip');
		$data['data_operator']=$this->m_lhp_wide_strip->getDataOperator();
		$data['data_downtime']=$this->m_lhp_wide_strip->getDataDowntime();
        $data['tb_transaction_operator'] = $this->m_lhp_wide_strip->getTrOperator($tanggal,$shift);
        $data['tb_transaction_downtime'] = $this->m_lhp_wide_strip->getTrDowntime($tanggal,$shift);
		$data['tb_transaction_hasil_produksi'] = $this->m_lhp_wide_strip->getTrHasilProduksi($tanggal,$shift);
		$data['tb_transaction_input_material'] = $this->m_lhp_wide_strip->getTrInputMaterial($tanggal,$shift);
		$data['tb_transaction_level_meltingpot'] = $this->m_lhp_wide_strip->getTrLevelMeltingPot($tanggal,$shift);
		$data['tb_transaction_setting'] = $this->m_lhp_wide_strip->getTrSetting($tanggal,$shift);
		$data['tb_transaction_dross'] = $this->m_lhp_wide_strip->getTrDross($tanggal,$shift);
		//var_dump($data['tb_transaction_setting']); die();

        // Tampilkan halaman edit
		$data['title'] = 'Checksheet Wide Strip';
		$data['username'] = $this->session->userdata('username');
		$this->load->view('tamplate/authHeader', $data);
        $this->load->view('edit_v_lhp_wide_strip', $data);
		$this->load->view('tamplate/authFooter');
    }
	
	public function tambah_aksi()
	{
		$tanggal							=$this->input->post('tanggal'); //tanggal adalah name pada view
		$shift								=$this->input->post('shift');
		$operator_rollmill					=$this->input->post('operator_rollmill');
		$operator_caster					=$this->input->post('operator_caster');
		$operator_support					=$this->input->post('operator_support');
		$pot_positif						=$this->input->post('pot_positif');
		$pot_negatif						=$this->input->post('pot_negatif');
		$pot_scrap_positif					=$this->input->post('pot_scrap_positif');
		$pot_scrap_negatif					=$this->input->post('pot_scrap_negatif');
		$pot_positif_ke_caster_1			=$this->input->post('pot_positif_ke_caster_1');
		$pot_positif_ke_caster_2			=$this->input->post('pot_positif_ke_caster_2');
		$pot_negatif_ke_caster_1			=$this->input->post('pot_negatif_ke_caster_1');
		$pot_negatif_ke_caster_2			=$this->input->post('pot_negatif_ke_caster_2');
		$pot_scrap_positif_ke_pot_positif_1	=$this->input->post('pot_scrap_positif_ke_pot_positif_1');
		$pot_scrap_positif_ke_pot_positif_2	=$this->input->post('pot_scrap_positif_ke_pot_positif_2');
		$pot_scrap_negatif_ke_pot_negatif_1	=$this->input->post('pot_scrap_negatif_ke_pot_negatif_1');
		$pot_scrap_negatif_ke_pot_negatif_2	=$this->input->post('pot_scrap_negatif_ke_pot_negatif_2');
		$manual_speed						=$this->input->post('manual_speed');
		$forming							=$this->input->post('forming');
		$feeder								=$this->input->post('feeder');
		$roll_1								=$this->input->post('roll_1');
		$roll_2								=$this->input->post('roll_2');
		$roll_3								=$this->input->post('roll_3');
		$roll_4								=$this->input->post('roll_4');
		$roll_5								=$this->input->post('roll_5');
		$roll_6								=$this->input->post('roll_6');
		$roll_7								=$this->input->post('roll_7');
		$cutter								=$this->input->post('cutter');
		$winder_1							=$this->input->post('winder_1');
		$winder_2							=$this->input->post('winder_2');
		$no1								=$this->input->post('no1');
		$winder1							=$this->input->post('winder1');
		$type1								=$this->input->post('type1');
		$panjang_m1							=$this->input->post('panjang_m1');
		$tebal_R1_mm1						=$this->input->post('tebal_R1_mm1');
		$tebal_L1_mm1						=$this->input->post('tebal_L1_mm1');
		$tebal_R2_mm1						=$this->input->post('tebal_R2_mm1');
		$tebal_L2_mm1						=$this->input->post('tebal_L2_mm1');
		$berat_kg1							=$this->input->post('berat_kg1');
		$no2								=$this->input->post('no2');
		$winder2							=$this->input->post('winder2');
		$type2								=$this->input->post('type2');
		$panjang_m2							=$this->input->post('panjang_m2');
		$tebal_R1_mm2						=$this->input->post('tebal_R1_mm2');
		$tebal_L1_mm2						=$this->input->post('tebal_L1_mm2');
		$tebal_R2_mm2						=$this->input->post('tebal_R2_mm2');
		$tebal_L2_mm2						=$this->input->post('tebal_L2_mm2');
		$berat_kg2							=$this->input->post('berat_kg2');
		$no3								=$this->input->post('no3');
		$winder3							=$this->input->post('winder3');
		$type3								=$this->input->post('type3');
		$panjang_m3							=$this->input->post('panjang_m3');
		$tebal_R1_mm3						=$this->input->post('tebal_R1_mm3');
		$tebal_L1_mm3						=$this->input->post('tebal_L1_mm3');
		$tebal_R2_mm3						=$this->input->post('tebal_R2_mm3');
		$tebal_L2_mm3						=$this->input->post('tebal_L2_mm3');
		$berat_kg3							=$this->input->post('berat_kg3');
		$no4								=$this->input->post('no4');
		$winder4							=$this->input->post('winder4');
		$type4								=$this->input->post('type4');
		$panjang_m4							=$this->input->post('panjang_m4');
		$tebal_R1_mm4						=$this->input->post('tebal_R1_mm4');
		$tebal_L1_mm4						=$this->input->post('tebal_L1_mm4');
		$tebal_R2_mm4						=$this->input->post('tebal_R2_mm4');
		$tebal_L2_mm4						=$this->input->post('tebal_L2_mm4');
		$berat_kg4							=$this->input->post('berat_kg4');
		$no5								=$this->input->post('no5');
		$winder5							=$this->input->post('winder5');
		$type5								=$this->input->post('type5');
		$panjang_m5							=$this->input->post('panjang_m5');
		$tebal_R1_mm5						=$this->input->post('tebal_R1_mm5');
		$tebal_L1_mm5						=$this->input->post('tebal_L1_mm5');
		$tebal_R2_mm5						=$this->input->post('tebal_R2_mm5');
		$tebal_L2_mm5						=$this->input->post('tebal_L2_mm5');
		$berat_kg5							=$this->input->post('berat_kg5');
		$no6								=$this->input->post('no6');
		$winder6							=$this->input->post('winder6');
		$type6								=$this->input->post('type6');
		$panjang_m6							=$this->input->post('panjang_m6');
		$tebal_R1_mm6						=$this->input->post('tebal_R1_mm6');
		$tebal_L1_mm6						=$this->input->post('tebal_L1_mm6');
		$tebal_R2_mm6						=$this->input->post('tebal_R2_mm6');
		$tebal_L2_mm6						=$this->input->post('tebal_L2_mm6');
		$berat_kg6							=$this->input->post('berat_kg6');
		$no7								=$this->input->post('no7');
		$winder7							=$this->input->post('winder7');
		$type7								=$this->input->post('type7');
		$panjang_m7							=$this->input->post('panjang_m7');
		$tebal_R1_mm7						=$this->input->post('tebal_R1_mm7');
		$tebal_L1_mm7						=$this->input->post('tebal_L1_mm7');
		$tebal_R2_mm7						=$this->input->post('tebal_R2_mm7');
		$tebal_L2_mm7						=$this->input->post('tebal_L2_mm7');
		$berat_kg7							=$this->input->post('berat_kg7');
		$no8								=$this->input->post('no8');
		$winder8							=$this->input->post('winder8');
		$type8								=$this->input->post('type8');
		$panjang_m8							=$this->input->post('panjang_m8');
		$tebal_R1_mm8						=$this->input->post('tebal_R1_mm8');
		$tebal_L1_mm8						=$this->input->post('tebal_L1_mm8');
		$tebal_R2_mm8						=$this->input->post('tebal_R2_mm8');
		$tebal_L2_mm8						=$this->input->post('tebal_L2_mm8');
		$berat_kg8							=$this->input->post('berat_kg8');
		$no9								=$this->input->post('no9');
		$winder9							=$this->input->post('winder9');
		$type9								=$this->input->post('type9');
		$panjang_m9							=$this->input->post('panjang_m9');
		$tebal_R1_mm9						=$this->input->post('tebal_R1_mm9');
		$tebal_L1_mm9						=$this->input->post('tebal_L1_mm9');
		$tebal_R2_mm9						=$this->input->post('tebal_R2_mm9');
		$tebal_L2_mm9						=$this->input->post('tebal_L2_mm9');
		$berat_kg9							=$this->input->post('berat_kg9');
		$no10								=$this->input->post('no10');
		$winder10							=$this->input->post('winder10');
		$type10								=$this->input->post('type10');
		$panjang_m10						=$this->input->post('panjang_m10');
		$tebal_R1_mm10						=$this->input->post('tebal_R1_mm10');
		$tebal_L1_mm10						=$this->input->post('tebal_L1_mm10');
		$tebal_R2_mm10						=$this->input->post('tebal_R2_mm10');
		$tebal_L2_mm10						=$this->input->post('tebal_L2_mm10');
		$berat_kg10							=$this->input->post('berat_kg10');
		$ch4_kg1							=$this->input->post('ch4_kg1');
		$ch5_kg1							=$this->input->post('ch5_kg1');
		$ma_ca_26_kg1 						=$this->input->post('ma_ca_26_kg1');
		$ma_sn_kg1							=$this->input->post('ma_sn_kg1');
		$scrap_kg1							=$this->input->post('scrap_kg1');
		$mlr_kg1							=$this->input->post('mlr_kg1'); 
		$ch4_kg2							=$this->input->post('ch4_kg2');
		$ch5_kg2							=$this->input->post('ch5_kg2');
		$ma_ca_26_kg2 						=$this->input->post('ma_ca_26_kg2');
		$ma_sn_kg2							=$this->input->post('ma_sn_kg2');
		$scrap_kg2							=$this->input->post('scrap_kg2');
		$mlr_kg2							=$this->input->post('mlr_kg2'); 
		$ch4_kg3							=$this->input->post('ch4_kg3');
		$ch5_kg3							=$this->input->post('ch5_kg3');
		$ma_ca_26_kg3 						=$this->input->post('ma_ca_26_kg3');
		$ma_sn_kg3							=$this->input->post('ma_sn_kg3');
		$scrap_kg3							=$this->input->post('scrap_kg3');
		$mlr_kg3							=$this->input->post('mlr_kg3');
		$ch4_kg4							=$this->input->post('ch4_kg4');
		$ch5_kg4							=$this->input->post('ch5_kg4');
		$ma_ca_26_kg4 						=$this->input->post('ma_ca_26_kg4');
		$ma_sn_kg4							=$this->input->post('ma_sn_kg4');
		$scrap_kg4							=$this->input->post('scrap_kg4');
		$mlr_kg4							=$this->input->post('mlr_kg4');
		$ch4_kg5							=$this->input->post('ch4_kg5');
		$ch5_kg5							=$this->input->post('ch5_kg5');
		$ma_ca_26_kg5 						=$this->input->post('ma_ca_26_kg5');
		$ma_sn_kg5							=$this->input->post('ma_sn_kg5');
		$scrap_kg5							=$this->input->post('scrap_kg5');
		$mlr_kg5							=$this->input->post('mlr_kg5');  
		$positif_aw_kg      				=$this->input->post('positif_aw_kg');
		$positif_ak_kg						=$this->input->post('positif_ak_kg');
		$negatif_aw_kg						=$this->input->post('negatif_aw_kg');
		$negatif_ak_kg						=$this->input->post('negatif_ak_kg');
		$scrap_positif_aw_kg				=$this->input->post('scrap_positif_aw_kg');
		$scrap_positif_ak_kg				=$this->input->post('scrap_positif_ak_kg');
		$scrap_negatif_aw_kg				=$this->input->post('scrap_negatif_aw_kg');
		$scrap_negatif_ak_kg				=$this->input->post('scrap_negatif_ak_kg');
		$nama_downtime1						=$this->input->post('nama_downtime1');
		$nama_downtime2						=$this->input->post('nama_downtime2');
		$nama_downtime3						=$this->input->post('nama_downtime3');
		$nama_downtime4						=$this->input->post('nama_downtime4');
		$nama_downtime5						=$this->input->post('nama_downtime5');
		$waktu1								=$this->input->post('waktu1');
		$waktu2								=$this->input->post('waktu2');
		$waktu3								=$this->input->post('waktu3');
		$waktu4								=$this->input->post('waktu4');
		$waktu5								=$this->input->post('waktu5');
		$start1								=$this->input->post('start1');
		$start2								=$this->input->post('start2');
		$start3								=$this->input->post('start3');
		$start4								=$this->input->post('start4');
		$start5								=$this->input->post('start5');
		$start6								=$this->input->post('start6');
		$start7								=$this->input->post('start7');
		$start8								=$this->input->post('start8');
		$start9								=$this->input->post('start9');
		$start10							=$this->input->post('start10');
		$finish1							=$this->input->post('finish1');
		$finish2							=$this->input->post('finish2');
		$finish3							=$this->input->post('finish3');
		$finish4							=$this->input->post('finish4');
		$finish5							=$this->input->post('finish5');
		$finish6							=$this->input->post('finish6');
		$finish7							=$this->input->post('finish7');
		$finish8							=$this->input->post('finish8');
		$finish9							=$this->input->post('finish9');
		$finish10							=$this->input->post('finish10');
		$bending1							=$this->input->post('bending1');
		$bending2							=$this->input->post('bending2');
		$bending3							=$this->input->post('bending3');
		$bending4							=$this->input->post('bending4');
		$bending5							=$this->input->post('bending5');
		$bending6							=$this->input->post('bending6');
		$bending7							=$this->input->post('bending7');
		$bending8							=$this->input->post('bending8');
		$bending9							=$this->input->post('bending9');
		$bending10							=$this->input->post('bending10');
		$pot_pos_kg							=$this->input->post('pot_pos_kg');
		$pot_neg_kg							=$this->input->post('pot_neg_kg');
		$pot_sc_pos_kg						=$this->input->post('pot_sc_pos_kg');
		$pot_sc_neg_kg						=$this->input->post('pot_sc_neg_kg');
		
		$data_tr_operator = array(
			'tanggal'			=> $tanggal,
			'shift'				=> $shift,
			'operator_rollmill'	=> $operator_rollmill,
			'operator_caster'	=> $operator_caster,
			'operator_support'	=> $operator_support
		);

		$data_tr_setting = array(
			'tanggal'							=> $tanggal,
			'shift'								=> $shift,
			'pot_positif'						=> $pot_positif,
			'pot_negatif'						=> $pot_negatif,
			'pot_scrap_positif'					=> $pot_scrap_positif,
			'pot_scrap_negatif'					=> $pot_scrap_negatif,
			'pot_positif_ke_caster_1'			=> $pot_positif_ke_caster_1,
			'pot_positif_ke_caster_2'			=> $pot_positif_ke_caster_2,
			'pot_negatif_ke_caster_1'			=> $pot_negatif_ke_caster_1,
			'pot_negatif_ke_caster_2'			=> $pot_negatif_ke_caster_2,
			'pot_scrap_positif_ke_pot_positif_1'=> $pot_scrap_positif_ke_pot_positif_1,
			'pot_scrap_positif_ke_pot_positif_2'=> $pot_scrap_positif_ke_pot_positif_2,
			'pot_scrap_negatif_ke_pot_negatif_1'=> $pot_scrap_negatif_ke_pot_negatif_1,
			'pot_scrap_negatif_ke_pot_negatif_2'=> $pot_scrap_negatif_ke_pot_negatif_2,
			'manual_speed'						=> $manual_speed,
			'forming'							=> $forming,
			'feeder'							=> $feeder,
			'roll_1'							=> $roll_1,
			'roll_2'							=> $roll_2,
			'roll_3'							=> $roll_3,
			'roll_4'							=> $roll_4,			
			'roll_5'							=> $roll_5,
			'roll_6'							=> $roll_6,
			'roll_7'							=> $roll_7,
			'cutter'							=> $cutter,
			'winder_1'							=> $winder_1,
			'winder_2'							=> $winder_2
			
		);

		$data_tr_hasil_produksi1 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no1,
			'start'			=> $start1,
			'winder'		=> $winder1,
			'type'			=> $type1,
			'panjang_m' 	=> $panjang_m1,
			'tebal_R1_mm' 	=> $tebal_R1_mm1,
			'tebal_L1_mm' 	=> $tebal_L1_mm1,
			'tebal_R2_mm' 	=> $tebal_R2_mm1,
			'tebal_L2_mm' 	=> $tebal_L2_mm1,
			'berat_kg'		=> $berat_kg1,
			'finish'		=> $finish1,
			'bending'		=> $bending1
		);

		$data_tr_hasil_produksi2 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no2,
			'start'			=> $start2,
			'winder'		=> $winder2,
			'type'			=> $type2,
			'panjang_m' 	=> $panjang_m2,
			'tebal_R1_mm' 	=> $tebal_R1_mm2,
			'tebal_L1_mm' 	=> $tebal_L1_mm2,
			'tebal_R2_mm' 	=> $tebal_R2_mm2,
			'tebal_L2_mm' 	=> $tebal_L2_mm2,
			'berat_kg'		=> $berat_kg2,
			'finish'		=> $finish2,
			'bending'		=> $bending2
		);
		
		$data_tr_hasil_produksi3 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no3,
			'start'			=> $start3,
			'winder'		=> $winder3,
			'type'			=> $type3,
			'panjang_m' 	=> $panjang_m3,
			'tebal_R1_mm' 	=> $tebal_R1_mm3,
			'tebal_L1_mm' 	=> $tebal_L1_mm3,
			'tebal_R2_mm' 	=> $tebal_R2_mm3,
			'tebal_L2_mm' 	=> $tebal_L2_mm3,
			'berat_kg'		=> $berat_kg3,
			'finish'		=> $finish3,
			'bending'		=> $bending3
		);

		$data_tr_hasil_produksi4 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no4,
			'start'			=> $start4,
			'winder'		=> $winder4,
			'type'			=> $type4,
			'panjang_m' 	=> $panjang_m4,
			'tebal_R1_mm' 	=> $tebal_R1_mm4,
			'tebal_L1_mm' 	=> $tebal_L1_mm4,
			'tebal_R2_mm' 	=> $tebal_R2_mm4,
			'tebal_L2_mm' 	=> $tebal_L2_mm4,
			'berat_kg'		=> $berat_kg4,
			'finish'		=> $finish4,
			'bending'		=> $bending4
		);

		$data_tr_hasil_produksi5 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no5,
			'start'			=> $start5,
			'winder'		=> $winder5,
			'type'			=> $type5,
			'panjang_m' 	=> $panjang_m5,
			'tebal_R1_mm' 	=> $tebal_R1_mm5,
			'tebal_L1_mm' 	=> $tebal_L1_mm5,
			'tebal_R2_mm' 	=> $tebal_R2_mm5,
			'tebal_L2_mm' 	=> $tebal_L2_mm5,
			'berat_kg'		=> $berat_kg5,
			'finish'		=> $finish5,
			'bending'		=> $bending5
		);

		$data_tr_hasil_produksi6 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no6,
			'start'			=> $start6,
			'winder'		=> $winder6,
			'type'			=> $type6,
			'panjang_m' 	=> $panjang_m6,
			'tebal_R1_mm' 	=> $tebal_R1_mm6,
			'tebal_L1_mm' 	=> $tebal_L1_mm6,
			'tebal_R2_mm' 	=> $tebal_R2_mm6,
			'tebal_L2_mm' 	=> $tebal_L2_mm6,
			'berat_kg'		=> $berat_kg6,
			'finish'		=> $finish6,
			'bending'		=> $bending6
		);

		$data_tr_hasil_produksi7 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no7,
			'start'			=> $start7,
			'winder'		=> $winder7,
			'type'			=> $type7,
			'panjang_m' 	=> $panjang_m7,
			'tebal_R1_mm' 	=> $tebal_R1_mm7,
			'tebal_L1_mm' 	=> $tebal_L1_mm7,
			'tebal_R2_mm' 	=> $tebal_R2_mm7,
			'tebal_L2_mm' 	=> $tebal_L2_mm7,
			'berat_kg'		=> $berat_kg7,
			'finish'		=> $finish7,
			'bending'		=> $bending7
		);

		$data_tr_hasil_produksi8 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no8,
			'start'			=> $start8,
			'winder'		=> $winder8,
			'type'			=> $type8,
			'panjang_m' 	=> $panjang_m8,
			'tebal_R1_mm' 	=> $tebal_R1_mm8,
			'tebal_L1_mm' 	=> $tebal_L1_mm8,
			'tebal_R2_mm' 	=> $tebal_R2_mm8,
			'tebal_L2_mm' 	=> $tebal_L2_mm8,
			'berat_kg'		=> $berat_kg8,
			'finish'		=> $finish8,
			'bending'		=> $bending8
		);

		$data_tr_hasil_produksi9 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no9,
			'start'			=> $start9,
			'winder'		=> $winder9,
			'type'			=> $type9,
			'panjang_m' 	=> $panjang_m9,
			'tebal_R1_mm' 	=> $tebal_R1_mm9,
			'tebal_L1_mm' 	=> $tebal_L1_mm9,
			'tebal_R2_mm' 	=> $tebal_R2_mm9,
			'tebal_L2_mm' 	=> $tebal_L2_mm9,
			'berat_kg'		=> $berat_kg9,
			'finish'		=> $finish9,
			'bending'		=> $bending9
		);

		$data_tr_hasil_produksi10 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no10,
			'start'			=> $start10,
			'winder'		=> $winder10,
			'type'			=> $type10,
			'panjang_m' 	=> $panjang_m10,
			'tebal_R1_mm' 	=> $tebal_R1_mm10,
			'tebal_L1_mm' 	=> $tebal_L1_mm10,
			'tebal_R2_mm' 	=> $tebal_R2_mm10,
			'tebal_L2_mm' 	=> $tebal_L2_mm10,
			'berat_kg'		=> $berat_kg10,
			'finish'		=> $finish10,
			'bending'		=> $bending10
		);

		$data_tr_input_material1 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg1,
			'ch5_kg'		=>$ch5_kg1,
			'ma_ca_26_kg'	=>$ma_ca_26_kg1,
			'ma_sn_kg'		=>$ma_sn_kg1,
			'scrap_kg'		=>$scrap_kg1,
			'mlr_kg'		=>$mlr_kg1	
		);

		$data_tr_input_material2 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg2,
			'ch5_kg'		=>$ch5_kg2,
			'ma_ca_26_kg'	=>$ma_ca_26_kg2,
			'ma_sn_kg'		=>$ma_sn_kg2,
			'scrap_kg'		=>$scrap_kg2,
			'mlr_kg'		=>$mlr_kg2	
		);

		$data_tr_input_material3 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg3,
			'ch5_kg'		=>$ch5_kg3,
			'ma_ca_26_kg'	=>$ma_ca_26_kg3,
			'ma_sn_kg'		=>$ma_sn_kg3,
			'scrap_kg'		=>$scrap_kg3,
			'mlr_kg'		=>$mlr_kg3	
		);

		$data_tr_input_material4 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg4,
			'ch5_kg'		=>$ch5_kg4,
			'ma_ca_26_kg'	=>$ma_ca_26_kg4,
			'ma_sn_kg'		=>$ma_sn_kg4,
			'scrap_kg'		=>$scrap_kg4,
			'mlr_kg'		=>$mlr_kg4	
		);

		$data_tr_input_material5 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg5,
			'ch5_kg'		=>$ch5_kg5,
			'ma_ca_26_kg'	=>$ma_ca_26_kg5,
			'ma_sn_kg'		=>$ma_sn_kg5,
			'scrap_kg'		=>$scrap_kg5,
			'mlr_kg'		=>$mlr_kg4	
		);

		$data_tr_level_meltingpot = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'positif_aw_kg'      => $positif_aw_kg,
			'positif_ak_kg'		 => $positif_ak_kg,
			'negatif_aw_kg'		 => $negatif_aw_kg,
			'negatif_ak_kg'		 => $negatif_ak_kg,
			'scrap_positif_aw_kg'=> $scrap_positif_aw_kg,
			'scrap_positif_ak_kg'=> $scrap_positif_ak_kg,
			'scrap_negatif_aw_kg'=> $scrap_negatif_aw_kg,
			'scrap_negatif_ak_kg'=> $scrap_negatif_ak_kg
		);

		$data_downtime1 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime1,
			'waktu_m'			 => $waktu1,
		);

		$data_downtime2 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime2,
			'waktu_m'			 => $waktu2,
		);

		$data_downtime3 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime3,
			'waktu_m'			 => $waktu3,
		);

		$data_downtime4 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime4,
			'waktu_m'			 => $waktu4,
		);

		$data_downtime5 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime5,
			'waktu_m'			 => $waktu4,
		);

		$data_tr_dross = array(
			'tanggal'			=> $tanggal,
			'shift'				=> $shift,
			'pot_pos_kg'		=> $pot_pos_kg,
			'pot_neg_kg'		=> $pot_neg_kg,
			'pot_sc_pos_kg'		=> $pot_sc_pos_kg,
			'pot_sc_neg_kg'		=> $pot_sc_neg_kg,
		);
		
		$this->m_lhp_wide_strip->input_data('tb_transaction_operator',$data_tr_operator);
		$this->m_lhp_wide_strip->input_data('tb_transaction_setting',$data_tr_setting);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi1);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi2);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi3);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi4);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi5);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi6);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi7);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi8);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi9);
		$this->m_lhp_wide_strip->input_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi10);
		$this->m_lhp_wide_strip->input_data('tb_transaction_input_material',$data_tr_input_material1);
		$this->m_lhp_wide_strip->input_data('tb_transaction_input_material',$data_tr_input_material2);
		$this->m_lhp_wide_strip->input_data('tb_transaction_input_material',$data_tr_input_material3);
		$this->m_lhp_wide_strip->input_data('tb_transaction_input_material',$data_tr_input_material4);
		$this->m_lhp_wide_strip->input_data('tb_transaction_input_material',$data_tr_input_material5);
		$this->m_lhp_wide_strip->input_data('tb_transaction_level_meltingpot',$data_tr_level_meltingpot);
		$this->m_lhp_wide_strip->input_data('tb_transaction_downtime',$data_downtime1);
		$this->m_lhp_wide_strip->input_data('tb_transaction_downtime',$data_downtime2);
		$this->m_lhp_wide_strip->input_data('tb_transaction_downtime',$data_downtime3);
		$this->m_lhp_wide_strip->input_data('tb_transaction_downtime',$data_downtime4);
		$this->m_lhp_wide_strip->input_data('tb_transaction_downtime',$data_downtime5);
		$this->m_lhp_wide_strip->input_data('tb_transaction_dross',$data_tr_dross);
		
		redirect ('lhp_wide_strip/summary');
	}

	public function update()
	{
		$tanggal							=$this->input->post('tanggal'); //tanggal adalah name pada view
		$shift								=$this->input->post('shift');
		$operator_rollmill					=$this->input->post('operator_rollmill');
		$operator_caster					=$this->input->post('operator_caster');
		$operator_support					=$this->input->post('operator_support');
		$pot_positif						=$this->input->post('pot_positif');
		$pot_negatif						=$this->input->post('pot_negatif');
		$pot_scrap_positif					=$this->input->post('pot_scrap_positif');
		$pot_scrap_negatif					=$this->input->post('pot_scrap_negatif');
		$pot_positif_ke_caster_1			=$this->input->post('pot_positif_ke_caster_1');
		$pot_positif_ke_caster_2			=$this->input->post('pot_positif_ke_caster_2');
		$pot_negatif_ke_caster_1			=$this->input->post('pot_negatif_ke_caster_1');
		$pot_negatif_ke_caster_2			=$this->input->post('pot_negatif_ke_caster_2');
		$pot_scrap_positif_ke_pot_positif_1	=$this->input->post('pot_scrap_positif_ke_pot_positif_1');
		$pot_scrap_positif_ke_pot_positif_2	=$this->input->post('pot_scrap_positif_ke_pot_positif_2');
		$pot_scrap_negatif_ke_pot_negatif_1	=$this->input->post('pot_scrap_negatif_ke_pot_negatif_1');
		$pot_scrap_negatif_ke_pot_negatif_2	=$this->input->post('pot_scrap_negatif_ke_pot_negatif_2');
		$manual_speed						=$this->input->post('manual_speed');
		$forming							=$this->input->post('forming');
		$feeder								=$this->input->post('feeder');
		$roll_1								=$this->input->post('roll_1');
		$roll_2								=$this->input->post('roll_2');
		$roll_3								=$this->input->post('roll_3');
		$roll_4								=$this->input->post('roll_4');
		$roll_5								=$this->input->post('roll_5');
		$roll_6								=$this->input->post('roll_6');
		$roll_7								=$this->input->post('roll_7');
		$cutter								=$this->input->post('cutter');
		$winder_1							=$this->input->post('winder_1');
		$winder_2							=$this->input->post('winder_2');
		$no1								=$this->input->post('no1');
		$winder1							=$this->input->post('winder1');
		$type1								=$this->input->post('type1');
		$panjang_m1							=$this->input->post('panjang_m1');
		//var_dump($this->input->post('panjang_m1')); die;
		$tebal_R1_mm1						=$this->input->post('tebal_R1_mm1');
		$tebal_L1_mm1						=$this->input->post('tebal_L1_mm1');
		$tebal_R2_mm1						=$this->input->post('tebal_R2_mm1');
		$tebal_L2_mm1						=$this->input->post('tebal_L2_mm1');
		$berat_kg1							=$this->input->post('berat_kg1');
		$no2								=$this->input->post('no2');
		$winder2							=$this->input->post('winder2');
		$type2								=$this->input->post('type2');
		$panjang_m2							=$this->input->post('panjang_m2');
		$tebal_R1_mm2						=$this->input->post('tebal_R1_mm2');
		$tebal_L1_mm2						=$this->input->post('tebal_L1_mm2');
		$tebal_R2_mm2						=$this->input->post('tebal_R2_mm2');
		$tebal_L2_mm2						=$this->input->post('tebal_L2_mm2');
		$berat_kg2							=$this->input->post('berat_kg2');
		$no3								=$this->input->post('no3');
		$winder3							=$this->input->post('winder3');
		$type3								=$this->input->post('type3');
		$panjang_m3							=$this->input->post('panjang_m3');
		$tebal_R1_mm3						=$this->input->post('tebal_R1_mm3');
		$tebal_L1_mm3						=$this->input->post('tebal_L1_mm3');
		$tebal_R2_mm3						=$this->input->post('tebal_R2_mm3');
		$tebal_L2_mm3						=$this->input->post('tebal_L2_mm3');
		$berat_kg3							=$this->input->post('berat_kg3');
		$no4								=$this->input->post('no4');
		$winder4							=$this->input->post('winder4');
		$type4								=$this->input->post('type4');
		$panjang_m4							=$this->input->post('panjang_m4');
		$tebal_R1_mm4						=$this->input->post('tebal_R1_mm4');
		$tebal_L1_mm4						=$this->input->post('tebal_L1_mm4');
		$tebal_R2_mm4						=$this->input->post('tebal_R2_mm4');
		$tebal_L2_mm4						=$this->input->post('tebal_L2_mm4');
		$berat_kg4							=$this->input->post('berat_kg4');
		$no5								=$this->input->post('no5');
		$winder5							=$this->input->post('winder5');
		$type5								=$this->input->post('type5');
		$panjang_m5							=$this->input->post('panjang_m5');
		$tebal_R1_mm5						=$this->input->post('tebal_R1_mm5');
		$tebal_L1_mm5						=$this->input->post('tebal_L1_mm5');
		$tebal_R2_mm5						=$this->input->post('tebal_R2_mm5');
		$tebal_L2_mm5						=$this->input->post('tebal_L2_mm5');
		$berat_kg5							=$this->input->post('berat_kg5');
		$no6								=$this->input->post('no6');
		$winder6							=$this->input->post('winder6');
		$type6								=$this->input->post('type6');
		$panjang_m6							=$this->input->post('panjang_m6');
		$tebal_R1_mm6						=$this->input->post('tebal_R1_mm6');
		$tebal_L1_mm6						=$this->input->post('tebal_L1_mm6');
		$tebal_R2_mm6						=$this->input->post('tebal_R2_mm6');
		$tebal_L2_mm6						=$this->input->post('tebal_L2_mm6');
		$berat_kg6							=$this->input->post('berat_kg6');
		$no7								=$this->input->post('no7');
		$winder7							=$this->input->post('winder7');
		$type7								=$this->input->post('type7');
		$panjang_m7							=$this->input->post('panjang_m7');
		$tebal_R1_mm7						=$this->input->post('tebal_R1_mm7');
		$tebal_L1_mm7						=$this->input->post('tebal_L1_mm7');
		$tebal_R2_mm7						=$this->input->post('tebal_R2_mm7');
		$tebal_L2_mm7						=$this->input->post('tebal_L2_mm7');
		$berat_kg7							=$this->input->post('berat_kg7');
		$no8								=$this->input->post('no8');
		$winder8							=$this->input->post('winder8');
		$type8								=$this->input->post('type8');
		$panjang_m8							=$this->input->post('panjang_m8');
		$tebal_R1_mm8						=$this->input->post('tebal_R1_mm8');
		$tebal_L1_mm8						=$this->input->post('tebal_L1_mm8');
		$tebal_R2_mm8						=$this->input->post('tebal_R2_mm8');
		$tebal_L2_mm8						=$this->input->post('tebal_L2_mm8');
		$berat_kg8							=$this->input->post('berat_kg8');
		$no9								=$this->input->post('no9');
		$winder9							=$this->input->post('winder9');
		$type9								=$this->input->post('type9');
		$panjang_m9							=$this->input->post('panjang_m9');
		$tebal_R1_mm9						=$this->input->post('tebal_R1_mm9');
		$tebal_L1_mm9						=$this->input->post('tebal_L1_mm9');
		$tebal_R2_mm9						=$this->input->post('tebal_R2_mm9');
		$tebal_L2_mm9						=$this->input->post('tebal_L2_mm9');
		$berat_kg9							=$this->input->post('berat_kg9');
		$no10								=$this->input->post('no10');
		$winder10							=$this->input->post('winder10');
		$type10								=$this->input->post('type10');
		$panjang_m10						=$this->input->post('panjang_m10');
		$tebal_R1_mm10						=$this->input->post('tebal_R1_mm10');
		$tebal_L1_mm10						=$this->input->post('tebal_L1_mm10');
		$tebal_R2_mm10						=$this->input->post('tebal_R2_mm10');
		$tebal_L2_mm10						=$this->input->post('tebal_L2_mm10');
		$berat_kg10							=$this->input->post('berat_kg10');
		$ch4_kg1							=$this->input->post('ch4_kg1');
		$ch5_kg1							=$this->input->post('ch5_kg1');
		$ma_ca_26_kg1 						=$this->input->post('ma_ca_26_kg1');
		$ma_sn_kg1							=$this->input->post('ma_sn_kg1');
		$scrap_kg1							=$this->input->post('scrap_kg1');
		$mlr_kg1							=$this->input->post('mlr_kg1'); 
		//var_dump($this->input->post('ma_sn_kg1')); die;
		$ch4_kg2							=$this->input->post('ch4_kg2');
		$ch5_kg2							=$this->input->post('ch5_kg2');
		$ma_ca_26_kg2 						=$this->input->post('ma_ca_26_kg2');
		$ma_sn_kg2							=$this->input->post('ma_sn_kg2');
		$scrap_kg2							=$this->input->post('scrap_kg2');
		$mlr_kg2							=$this->input->post('mlr_kg2'); 
		$ch4_kg3							=$this->input->post('ch4_kg3');
		$ch5_kg3							=$this->input->post('ch5_kg3');
		$ma_ca_26_kg3 						=$this->input->post('ma_ca_26_kg3');
		$ma_sn_kg3							=$this->input->post('ma_sn_kg3');
		$scrap_kg3							=$this->input->post('scrap_kg3');
		$mlr_kg3							=$this->input->post('mlr_kg3');
		$ch4_kg4							=$this->input->post('ch4_kg4');
		$ch5_kg4							=$this->input->post('ch5_kg4');
		$ma_ca_26_kg4 						=$this->input->post('ma_ca_26_kg4');
		$ma_sn_kg4							=$this->input->post('ma_sn_kg4');
		$scrap_kg4							=$this->input->post('scrap_kg4');
		$mlr_kg4							=$this->input->post('mlr_kg4');
		$ch4_kg5							=$this->input->post('ch4_kg5');
		$ch5_kg5							=$this->input->post('ch5_kg5');
		$ma_ca_26_kg5 						=$this->input->post('ma_ca_26_kg5');
		$ma_sn_kg5							=$this->input->post('ma_sn_kg5');
		$scrap_kg5							=$this->input->post('scrap_kg5');
		$mlr_kg5							=$this->input->post('mlr_kg5');  
		$positif_aw_kg      				=$this->input->post('positif_aw_kg');
		$positif_ak_kg						=$this->input->post('positif_ak_kg');
		$negatif_aw_kg						=$this->input->post('negatif_aw_kg');
		$negatif_ak_kg						=$this->input->post('negatif_ak_kg');
		$scrap_positif_aw_kg				=$this->input->post('scrap_positif_aw_kg');
		$scrap_positif_ak_kg				=$this->input->post('scrap_positif_ak_kg');
		$scrap_negatif_aw_kg				=$this->input->post('scrap_negatif_aw_kg');
		$scrap_negatif_ak_kg				=$this->input->post('scrap_negatif_ak_kg');
		$nama_downtime1						=$this->input->post('nama_downtime1');
		$nama_downtime2						=$this->input->post('nama_downtime2');
		$nama_downtime3						=$this->input->post('nama_downtime3');
		$nama_downtime4						=$this->input->post('nama_downtime4');
		$nama_downtime5						=$this->input->post('nama_downtime5');
		$waktu1								=$this->input->post('waktu1');
		$waktu2								=$this->input->post('waktu2');
		$waktu3								=$this->input->post('waktu3');
		$waktu4								=$this->input->post('waktu4');
		$waktu5								=$this->input->post('waktu5');
		$start1								=$this->input->post('start1');
		$start2								=$this->input->post('start2');
		$start3								=$this->input->post('start3');
		$start4								=$this->input->post('start4');
		$start5								=$this->input->post('start5');
		$start6								=$this->input->post('start6');
		$start7								=$this->input->post('start7');
		$start8								=$this->input->post('start8');
		$start9								=$this->input->post('start9');
		$start10							=$this->input->post('start10');
		$finish1							=$this->input->post('finish1');
		$finish2							=$this->input->post('finish2');
		$finish3							=$this->input->post('finish3');
		$finish4							=$this->input->post('finish4');
		$finish5							=$this->input->post('finish5');
		$finish6							=$this->input->post('finish6');
		$finish7							=$this->input->post('finish7');
		$finish8							=$this->input->post('finish8');
		$finish9							=$this->input->post('finish9');
		$finish10							=$this->input->post('finish10');
		$bending1							=$this->input->post('bending1');
		$bending2							=$this->input->post('bending2');
		$bending3							=$this->input->post('bending3');
		$bending4							=$this->input->post('bending4');
		$bending5							=$this->input->post('bending5');
		$bending6							=$this->input->post('bending6');
		$bending7							=$this->input->post('bending7');
		$bending8							=$this->input->post('bending8');
		$bending9							=$this->input->post('bending9');
		$bending10							=$this->input->post('bending10');
		$pot_pos_kg							=$this->input->post('pot_pos_kg');
		$pot_neg_kg							=$this->input->post('pot_neg_kg');
		$pot_sc_pos_kg						=$this->input->post('pot_sc_pos_kg');
		$pot_sc_neg_kg						=$this->input->post('pot_sc_neg_kg');
		//var_dump($pot_pos_kg); die();
		$id_transaction_operator			=$this->input->post('id_transaction_operator');
		$id_transaction_setting				=$this->input->post('id_transaction_setting');
		$id_transaction_input_material1		=$this->input->post('id_transaction_input_material1');
		$id_transaction_input_material2		=$this->input->post('id_transaction_input_material2');
		$id_transaction_input_material3		=$this->input->post('id_transaction_input_material3');
		$id_transaction_input_material4		=$this->input->post('id_transaction_input_material4');
		$id_transaction_input_material5		=$this->input->post('id_transaction_input_material5');
		$id_transaction_dross				=$this->input->post('id_transaction_dross');
		$id_level_meltingpot				=$this->input->post('id_level_meltingpot');
		$id_downtime1						=$this->input->post('id_downtime1');
		$id_downtime2						=$this->input->post('id_downtime2');
		$id_downtime3						=$this->input->post('id_downtime3');
		$id_downtime4						=$this->input->post('id_downtime4');
		$id_downtime5						=$this->input->post('id_downtime5');
		$id_transaction_operator			=$this->input->post('id_transaction_operator');
		$id_transaction_hasil_produksi1		=$this->input->post('id_transaction_hasil_produksi1');
		$id_transaction_hasil_produksi2		=$this->input->post('id_transaction_hasil_produksi2');
		$id_transaction_hasil_produksi3		=$this->input->post('id_transaction_hasil_produksi3');
		$id_transaction_hasil_produksi4		=$this->input->post('id_transaction_hasil_produksi4');
		$id_transaction_hasil_produksi5		=$this->input->post('id_transaction_hasil_produksi5');
		$id_transaction_hasil_produksi6		=$this->input->post('id_transaction_hasil_produksi6');
		$id_transaction_hasil_produksi7		=$this->input->post('id_transaction_hasil_produksi7');
		$id_transaction_hasil_produksi8		=$this->input->post('id_transaction_hasil_produksi8');
		$id_transaction_hasil_produksi9		=$this->input->post('id_transaction_hasil_produksi9');
		$id_transaction_hasil_produksi10	=$this->input->post('id_transaction_hasil_produksi10');
		//var_dump($id_transaction_setting); die();

		$data_tr_operator = array(
			'tanggal'			=> $tanggal,
			'shift'				=> $shift,
			'operator_rollmill'	=> $operator_rollmill,
			'operator_caster'	=> $operator_caster,
			'operator_support'	=> $operator_support
		);

		$data_tr_setting = array(
			'tanggal'							=> $tanggal,
			'shift'								=> $shift,
			'pot_positif'						=> $pot_positif,
			'pot_negatif'						=> $pot_negatif,
			'pot_scrap_positif'					=> $pot_scrap_positif,
			'pot_scrap_negatif'					=> $pot_scrap_negatif,
			'pot_positif_ke_caster_1'			=> $pot_positif_ke_caster_1,
			'pot_positif_ke_caster_2'			=> $pot_positif_ke_caster_2,
			'pot_negatif_ke_caster_1'			=> $pot_negatif_ke_caster_1,
			'pot_negatif_ke_caster_2'			=> $pot_negatif_ke_caster_2,
			'pot_scrap_positif_ke_pot_positif_1'=> $pot_scrap_positif_ke_pot_positif_1,
			'pot_scrap_positif_ke_pot_positif_2'=> $pot_scrap_positif_ke_pot_positif_2,
			'pot_scrap_negatif_ke_pot_negatif_1'=> $pot_scrap_negatif_ke_pot_negatif_1,
			'pot_scrap_negatif_ke_pot_negatif_2'=> $pot_scrap_negatif_ke_pot_negatif_2,
			'manual_speed'						=> $manual_speed,
			'forming'							=> $forming,
			'feeder'							=> $feeder,
			'roll_1'							=> $roll_1,
			'roll_2'							=> $roll_2,
			'roll_3'							=> $roll_3,
			'roll_4'							=> $roll_4,			
			'roll_5'							=> $roll_5,
			'roll_6'							=> $roll_6,
			'roll_7'							=> $roll_7,
			'cutter'							=> $cutter,
			'winder_1'							=> $winder_1,
			'winder_2'							=> $winder_2
			
		);

		$data_tr_hasil_produksi1 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi1,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no1,
			'start'			=> $start1,
			'winder'		=> $winder1,
			'type'			=> $type1,
			'panjang_m' 	=> $panjang_m1,
			'tebal_R1_mm' 	=> $tebal_R1_mm1,
			'tebal_L1_mm' 	=> $tebal_L1_mm1,
			'tebal_R2_mm' 	=> $tebal_R2_mm1,
			'tebal_L2_mm' 	=> $tebal_L2_mm1,
			'berat_kg'		=> $berat_kg1,
			'finish'		=> $finish1,
			'bending'		=> $bending1
		);

		$data_tr_hasil_produksi2 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi2,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no2,
			'start'			=> $start2,
			'winder'		=> $winder2,
			'type'			=> $type2,
			'panjang_m' 	=> $panjang_m2,
			'tebal_R1_mm' 	=> $tebal_R1_mm2,
			'tebal_L1_mm' 	=> $tebal_L1_mm2,
			'tebal_R2_mm' 	=> $tebal_R2_mm2,
			'tebal_L2_mm' 	=> $tebal_L2_mm2,
			'berat_kg'		=> $berat_kg2,
			'finish'		=> $finish2,
			'bending'		=> $bending2
		);
		
		$data_tr_hasil_produksi3 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi3,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no3,
			'start'			=> $start3,
			'winder'		=> $winder3,
			'type'			=> $type3,
			'panjang_m' 	=> $panjang_m3,
			'tebal_R1_mm' 	=> $tebal_R1_mm3,
			'tebal_L1_mm' 	=> $tebal_L1_mm3,
			'tebal_R2_mm' 	=> $tebal_R2_mm3,
			'tebal_L2_mm' 	=> $tebal_L2_mm3,
			'berat_kg'		=> $berat_kg3,
			'finish'		=> $finish3,
			'bending'		=> $bending3
		);

		$data_tr_hasil_produksi4 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi4,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no4,
			'start'			=> $start4,
			'winder'		=> $winder4,
			'type'			=> $type4,
			'panjang_m' 	=> $panjang_m4,
			'tebal_R1_mm' 	=> $tebal_R1_mm4,
			'tebal_L1_mm' 	=> $tebal_L1_mm4,
			'tebal_R2_mm' 	=> $tebal_R2_mm4,
			'tebal_L2_mm' 	=> $tebal_L2_mm4,
			'berat_kg'		=> $berat_kg4,
			'finish'		=> $finish4,
			'bending'		=> $bending4
		);

		$data_tr_hasil_produksi5 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi5,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no5,
			'start'			=> $start5,
			'winder'		=> $winder5,
			'type'			=> $type5,
			'panjang_m' 	=> $panjang_m5,
			'tebal_R1_mm' 	=> $tebal_R1_mm5,
			'tebal_L1_mm' 	=> $tebal_L1_mm5,
			'tebal_R2_mm' 	=> $tebal_R2_mm5,
			'tebal_L2_mm' 	=> $tebal_L2_mm5,
			'berat_kg'		=> $berat_kg5,
			'finish'		=> $finish5,
			'bending'		=> $bending5
		);

		$data_tr_hasil_produksi6 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi6,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no6,
			'start'			=> $start6,
			'winder'		=> $winder6,
			'type'			=> $type6,
			'panjang_m' 	=> $panjang_m6,
			'tebal_R1_mm' 	=> $tebal_R1_mm6,
			'tebal_L1_mm' 	=> $tebal_L1_mm6,
			'tebal_R2_mm' 	=> $tebal_R2_mm6,
			'tebal_L2_mm' 	=> $tebal_L2_mm6,
			'berat_kg'		=> $berat_kg6,
			'finish'		=> $finish6,
			'bending'		=> $bending6
		);

		$data_tr_hasil_produksi7 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi7,			
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no7,
			'start'			=> $start7,
			'winder'		=> $winder7,
			'type'			=> $type7,
			'panjang_m' 	=> $panjang_m7,
			'tebal_R1_mm' 	=> $tebal_R1_mm7,
			'tebal_L1_mm' 	=> $tebal_L1_mm7,
			'tebal_R2_mm' 	=> $tebal_R2_mm7,
			'tebal_L2_mm' 	=> $tebal_L2_mm7,
			'berat_kg'		=> $berat_kg7,
			'finish'		=> $finish7,
			'bending'		=> $bending7
		);

		$data_tr_hasil_produksi8 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi8,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no8,
			'start'			=> $start8,
			'winder'		=> $winder8,
			'type'			=> $type8,
			'panjang_m' 	=> $panjang_m8,
			'tebal_R1_mm' 	=> $tebal_R1_mm8,
			'tebal_L1_mm' 	=> $tebal_L1_mm8,
			'tebal_R2_mm' 	=> $tebal_R2_mm8,
			'tebal_L2_mm' 	=> $tebal_L2_mm8,
			'berat_kg'		=> $berat_kg8,
			'finish'		=> $finish8,
			'bending'		=> $bending8
		);

		$data_tr_hasil_produksi9 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi9,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no9,
			'start'			=> $start9,
			'winder'		=> $winder9,
			'type'			=> $type9,
			'panjang_m' 	=> $panjang_m9,
			'tebal_R1_mm' 	=> $tebal_R1_mm9,
			'tebal_L1_mm' 	=> $tebal_L1_mm9,
			'tebal_R2_mm' 	=> $tebal_R2_mm9,
			'tebal_L2_mm' 	=> $tebal_L2_mm9,
			'berat_kg'		=> $berat_kg9,
			'finish'		=> $finish9,
			'bending'		=> $bending9
		);

		$data_tr_hasil_produksi10 = array(
			'id_transaction_hasil_produksi'=>$id_transaction_hasil_produksi10,
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'no'			=> $no10,
			'start'			=> $start10,
			'winder'		=> $winder10,
			'type'			=> $type10,
			'panjang_m' 	=> $panjang_m10,
			'tebal_R1_mm' 	=> $tebal_R1_mm10,
			'tebal_L1_mm' 	=> $tebal_L1_mm10,
			'tebal_R2_mm' 	=> $tebal_R2_mm10,
			'tebal_L2_mm' 	=> $tebal_L2_mm10,
			'berat_kg'		=> $berat_kg10,
			'finish'		=> $finish10,
			'bending'		=> $bending10
		);

		$data_tr_input_material1 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg1,
			'ch5_kg'		=>$ch5_kg1,
			'ma_ca_26_kg'	=>$ma_ca_26_kg1,
			'ma_sn_kg'		=>$ma_sn_kg1,
			'scrap_kg'		=>$scrap_kg1,
			'mlr_kg'		=>$mlr_kg1	
		);
			//var_dump($data_tr_input_material1); die;
		$data_tr_input_material2 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg2,
			'ch5_kg'		=>$ch5_kg2,
			'ma_ca_26_kg'	=>$ma_ca_26_kg2,
			'ma_sn_kg'		=>$ma_sn_kg2,
			'scrap_kg'		=>$scrap_kg2,
			'mlr_kg'		=>$mlr_kg2	
		);

		$data_tr_input_material3 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg3,
			'ch5_kg'		=>$ch5_kg3,
			'ma_ca_26_kg'	=>$ma_ca_26_kg3,
			'ma_sn_kg'		=>$ma_sn_kg3,
			'scrap_kg'		=>$scrap_kg3,
			'mlr_kg'		=>$mlr_kg3	
		);

		$data_tr_input_material4 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg4,
			'ch5_kg'		=>$ch5_kg4,
			'ma_ca_26_kg'	=>$ma_ca_26_kg4,
			'ma_sn_kg'		=>$ma_sn_kg4,
			'scrap_kg'		=>$scrap_kg4,
			'mlr_kg'		=>$mlr_kg4	
		);

		$data_tr_input_material5 = array(
			'tanggal'		=> $tanggal,
			'shift'			=> $shift,
			'ch4_kg'		=>$ch4_kg5,
			'ch5_kg'		=>$ch5_kg5,
			'ma_ca_26_kg'	=>$ma_ca_26_kg5,
			'ma_sn_kg'		=>$ma_sn_kg5,
			'scrap_kg'		=>$scrap_kg5,
			'mlr_kg'		=>$mlr_kg4	
		);

		$data_tr_level_meltingpot = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'positif_aw_kg'      => $positif_aw_kg,
			'positif_ak_kg'		 => $positif_ak_kg,
			'negatif_aw_kg'		 => $negatif_aw_kg,
			'negatif_ak_kg'		 => $negatif_ak_kg,
			'scrap_positif_aw_kg'=> $scrap_positif_aw_kg,
			'scrap_positif_ak_kg'=> $scrap_positif_ak_kg,
			'scrap_negatif_aw_kg'=> $scrap_negatif_aw_kg,
			'scrap_negatif_ak_kg'=> $scrap_negatif_ak_kg
		);

		$data_downtime1 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime1,
			'waktu_m'			 => $waktu1,
		);

		$data_downtime2 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime2,
			'waktu_m'			 => $waktu2,
		);

		$data_downtime3 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime3,
			'waktu_m'			 => $waktu3,
		);

		$data_downtime4 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime4,
			'waktu_m'			 => $waktu4,
		);

		$data_downtime5 = array(
			'tanggal'			 => $tanggal,
			'shift'				 => $shift,
			'nama_downtime'		 => $nama_downtime5,
			'waktu_m'			 => $waktu4,
		);

		$data_tr_dross = array(
			'tanggal'			=> $tanggal,
			'shift'				=> $shift,
			'pot_pos_kg'		=> $pot_pos_kg,
			'pot_neg_kg'		=> $pot_neg_kg,
			'pot_sc_pos_kg'		=> $pot_sc_pos_kg,
			'pot_sc_neg_kg'		=> $pot_sc_neg_kg,
		);
		
		$this->load->model('m_lhp_wide_strip');
		$this->m_lhp_wide_strip->update_data('tb_transaction_operator',$data_tr_operator,$id_transaction_operator,'id_transaction_operator');
		$this->m_lhp_wide_strip->update_data('tb_transaction_setting',$data_tr_setting, $this->input->post('id_transaction_setting'),'id_transaction_setting');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi1 ,$id_transaction_hasil_produksi1,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi2,$id_transaction_hasil_produksi2,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi3,$id_transaction_hasil_produksi3,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi4,$id_transaction_hasil_produksi4,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi5,$id_transaction_hasil_produksi5,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi6,$id_transaction_hasil_produksi6,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi7,$id_transaction_hasil_produksi7,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi8,$id_transaction_hasil_produksi8,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi9,$id_transaction_hasil_produksi9,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_hasil_produksi',$data_tr_hasil_produksi10,$id_transaction_hasil_produksi10,'id_transaction_hasil_produksi');
		$this->m_lhp_wide_strip->update_data('tb_transaction_input_material',$data_tr_input_material1,$id_transaction_input_material1,'id_transaction_input_material');
		$this->m_lhp_wide_strip->update_data('tb_transaction_input_material',$data_tr_input_material2,$id_transaction_input_material2,'id_transaction_input_material');
		$this->m_lhp_wide_strip->update_data('tb_transaction_input_material',$data_tr_input_material3,$id_transaction_input_material3,'id_transaction_input_material');
		$this->m_lhp_wide_strip->update_data('tb_transaction_input_material',$data_tr_input_material4,$id_transaction_input_material4,'id_transaction_input_material');
		$this->m_lhp_wide_strip->update_data('tb_transaction_input_material',$data_tr_input_material5,$id_transaction_input_material5,'id_transaction_input_material');
		$this->m_lhp_wide_strip->update_data('tb_transaction_level_meltingpot',$data_tr_level_meltingpot, $id_level_meltingpot,'id_level_meltingpot');
		$this->m_lhp_wide_strip->update_data('tb_transaction_downtime',$data_downtime1,$id_downtime1,'id_downtime');
		$this->m_lhp_wide_strip->update_data('tb_transaction_downtime',$data_downtime2,$id_downtime2,'id_downtime');
		$this->m_lhp_wide_strip->update_data('tb_transaction_downtime',$data_downtime3,$id_downtime3,'id_downtime');
		$this->m_lhp_wide_strip->update_data('tb_transaction_downtime',$data_downtime4,$id_downtime4,'id_downtime');
		$this->m_lhp_wide_strip->update_data('tb_transaction_downtime',$data_downtime5,$id_downtime5,'id_downtime');
		$this->m_lhp_wide_strip->update_data('tb_transaction_dross',$data_tr_dross,$id_transaction_dross,'id_transaction_dross');
		redirect ('lhp_wide_strip/summary');
	}

	public function delete($tanggal='',$shift='') {
        // Panggil model untuk mengambil data
		$this->load->model('m_lhp_wide_strip');
		$this->m_lhp_wide_strip->delete_data('tb_transaction_operator',$shift, $tanggal);
		$this->m_lhp_wide_strip->delete_data('tb_transaction_setting',$shift, $tanggal);
		$this->m_lhp_wide_strip->delete_data('tb_transaction_input_material',$shift, $tanggal);
		$this->m_lhp_wide_strip->delete_data('tb_transaction_level_meltingpot',$shift, $tanggal);
		$this->m_lhp_wide_strip->delete_data('tb_transaction_downtime',$shift, $tanggal);
		$this->m_lhp_wide_strip->delete_data('tb_transaction_dross',$shift, $tanggal);
		redirect ('lhp_wide_strip/summary');
	}

}