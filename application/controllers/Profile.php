<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function getdata()
	{
		$data['username'] = $this->session->userdata('username');
		$data['nama'] = $this->session->userdata('nama');
		$dataT['title'] = 'Profile';
		$this->load->view('tamplate/authHeader', $data);
		$this->load->view('profile',$data);
		$this->load->view('tamplate/authFooter');
	}
}
