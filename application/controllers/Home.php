<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('auth');
		$this->auth->cek_login();
		$this->load->model('m_lhp_wide_strip');
		$this->load->library('session');
	}

	public function index()
	{
		$data['tr_operator']=$this->m_lhp_wide_strip->summary();
		$data['title'] = 'Summary LHP WS';
		$data['username'] = $this->session->userdata('username');
		$this->load->view('tamplate/authHeader', $data);
		$this->load->view('v_summary_lhp_ws',$data);
		$this->load->view('tamplate/authFooter');
		
	}



}