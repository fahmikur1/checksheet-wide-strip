<?php

class m_lhp_wide_strip extends CI_Model
{
    public function input_data($tabel,$data)
    {
        $this->db->insert($tabel, $data);
    }

    public function update_data($tabel,$data,$id,$nama_field)
    {
        $this->db->where($nama_field, $id);
        $this->db->update($tabel, $data);
    }

    public function delete_data($tabel,$shift,$tanggal)
    {
        $this->db->where('shift',$shift);
        $this->db->where('tanggal', $tanggal);
        $this->db->delete($tabel);
    }
      
    public function getDataDowntime()
    {
        $query = $this->db->query("SELECT * FROM tb_data_downtime");
        return $query->result_array();
    }

    public function getDataOperator()
    {
        $query = $this->db->query("SELECT * FROM tb_data_operator");
        return $query->result_array();
    }

    public function summary()
    {
        $query = $this->db->query("SELECT * FROM tb_transaction_operator");
        return $query->result_array();
    }

    public function getTrOperator($tanggal,$shift) {
        $query = $this->db->get_where('tb_transaction_operator', array('tanggal' => $tanggal, 'shift'=> $shift));
        return $query->result_object();
    }

    public function getTrDowntime($tanggal,$shift) {
        $query = $this->db->get_where('tb_transaction_downtime', array('tanggal' => $tanggal, 'shift'=> $shift));
        return $query->result_object();
    }

    public function getTrHasilProduksi($tanggal,$shift) {
        $query = $this->db->get_where('tb_transaction_hasil_produksi', array('tanggal' => $tanggal, 'shift'=> $shift));
         return $query->result_object();
    }

    public function getTrInputMaterial($tanggal,$shift) {
        $query = $this->db->get_where('tb_transaction_input_material', array('tanggal' => $tanggal, 'shift'=> $shift));
        return $query->result_object();
    }

    public function getTrLevelMeltingPot($tanggal,$shift) {
        $query = $this->db->get_where('tb_transaction_level_meltingpot', array('tanggal' => $tanggal, 'shift'=> $shift));
        return $query->result_object();
    }

    public function getTrSetting($tanggal,$shift) {
        $query = $this->db->get_where('tb_transaction_setting', array('tanggal' => $tanggal, 'shift'=> $shift));
        return $query->result_object();
    }

    public function getTrDross($tanggal,$shift) {
        $query = $this->db->get_where('tb_transaction_dross', array('tanggal' => $tanggal, 'shift'=> $shift));
        return $query->result_object();
    }

}