<form class="alert mt-5" method="post" action="<?php echo base_url('lhp_wide_strip/update'); ?>">
  <div class="container">
    <table class="table table-bordered border-muted">
      <tbody>
        <tr style="border:none;">
          <td style="border:none;"><img src="<?= base_url('assets/images/iPTCBI.png') ?>" alt="logo CBI" style="width: 200px;"></td>
          <td style="border:none;">
            <h1 class="alert " style="border: none; text-align: left;"><b>CHECKSHEET WIDE STRIP</b></h1>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  </div>



  <div class="container">
    <div class="row">
      <?php
      foreach ($tb_transaction_operator as $to) { ?>
        <input value="<?php echo $to->id_transaction_operator; ?>" type="hidden" name='id_transaction_operator' class="form-control">
        <div class="col-md-2">
          <div class="form-group">
            <label>Tanggal</label>
            <input value="<?php echo $to->tanggal; ?>" type="date" name='tanggal' class="form-control">
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Shift</label>
            <input value="<?php echo $to->shift; ?>" type="number" name='shift' class="form-control" style="text-align: center">
          </div>
        </div>
      <?php } ?>
      <div class="col-md-6"></div>
        <div class="col-md-2">
          <a href="http://localhost/lhp_wide_strip/home" class="btn btn-sm btn-link d-flex justify-content-end fs-4"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
        </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label>Operator Roll Mill</label>
          <select class="form-select" name="operator_rollmill" aria-label="Default select example">
            <option selected>--Pilih nama operator--</option>
            <?php
            foreach ($data_operator as $d_operator) {
              $selected = "";
              foreach ($tb_transaction_operator as $to) {
                if ($to->operator_rollmill == $d_operator['operator']) {
                  $selected = "selected";
                }
              } ?>
              <option <?php echo $selected; ?>><?php echo $d_operator['operator']; ?> </option>
            <?php } ?>
          </select>
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label>Operator Caster</label>
          <select class="form-select" name="operator_caster" aria-label="Default select example">
            <option selected>--Pilih nama operator--</option>
            <?php
            foreach ($data_operator as $d_operator) {
              $selected = "";
              foreach ($tb_transaction_operator as $to) {
                if ($to->operator_caster == $d_operator['operator']) {
                  $selected = "selected";
                }
              } ?>
              <option <?php echo $selected; ?>><?php echo $d_operator['operator']; ?> </option>
            <?php } ?>
          </select>
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label>Operator Feeder</label>
          <select class="form-select" name="operator_support" aria-label="Default select example">
            <option selected>--Pilih nama operator--</option>
            <?php
            foreach ($data_operator as $d_operator) {
              $selected = "";
              foreach ($tb_transaction_operator as $to) {
                if ($to->operator_support == $d_operator['operator']) {
                  $selected = "selected";
                }
              } ?>
              <option <?php echo $selected; ?>><?php echo $d_operator['operator']; ?> </option>
            <?php } ?>
          </select>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Setting Parameter Proses</u></b></h5>

    <div class="container">
      <h6 class="alert fs-4 text-center " style="border: none"><b>Temperatur Melting Pot</b></h6>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->id_transaction_setting; ?>" type="hidden" name='id_transaction_setting' class="form-control">
            <?php } ?>
            <label>Positif</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_positif; ?>" type="number" name='pot_positif' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Negatif</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_negatif; ?>" type="number" name='pot_negatif' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Scrap Positif</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_scrap_positif; ?>" type="number" name='pot_scrap_positif' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Scrap Negatif</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_scrap_negatif; ?>" type="number" name='pot_scrap_negatif' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <h6 class="alert fs-4 text-center " style="border: none"><b>Temperatur Feedline</b></h6>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Pot Positif ke Caster</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_positif_ke_caster_1; ?>" type="number" name='pot_positif_ke_caster_1' class="form-control" style="text-align: center">
            <?php } ?>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_positif_ke_caster_2; ?>" type="number" name='pot_positif_ke_caster_2' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Pot Negatif ke Caster</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_negatif_ke_caster_1; ?>" type="number" name='pot_negatif_ke_caster_1' class="form-control" style="text-align: center">
            <?php } ?>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_negatif_ke_caster_2; ?>" type="number" name='pot_negatif_ke_caster_2' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Pot Scrap Positif ke Pot Positif</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_scrap_positif_ke_pot_positif_1; ?>" type="number" name='pot_scrap_positif_ke_pot_positif_1' class="form-control" style="text-align: center">
            <?php } ?>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_scrap_positif_ke_pot_positif_2; ?>" type="number" name='pot_scrap_positif_ke_pot_positif_2' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Pot Scrap Negatif ke Pot Negatif </label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_scrap_negatif_ke_pot_negatif_1; ?>" type="number" name='pot_scrap_negatif_ke_pot_negatif_1' class="form-control" style="text-align: center">
            <?php } ?>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->pot_scrap_negatif_ke_pot_negatif_2; ?>" type="number" name='pot_scrap_negatif_ke_pot_negatif_2' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <h6 class="alert fs-4 text-center " style="border: none"><b>Setting Rollmill</b></h6>

      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Manual Speed (m/min)</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->manual_speed; ?>" type="number" step='0.01' name='manual_speed' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <h6 class="alert fs-4 text-left " style="border: none">Ratio Speed (m/min)</h6>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-2">
          <div class="form-group">
            <label>Forming</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->forming; ?>" type="number" step='0.001' name='forming' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Feeder</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->feeder; ?>" type="number" step='0.001' name='feeder' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Roll 1</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->roll_1; ?>" type="number" step='0.001' name='roll_1' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Roll 2</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->roll_2; ?>" type="number" step='0.001' name='roll_2' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Roll 3</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->roll_3; ?>" type="number" step='0.001' name='roll_3' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Roll 4</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->roll_4; ?>" type="number" step='0.001' name='roll_4' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Roll 5</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->roll_5; ?>" type="number" step='0.001' name='roll_5' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Roll 6</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->roll_6; ?>" type="number" step='0.001' name='roll_6' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Roll 7</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->roll_7; ?>" type="number" step='0.001' name='roll_7' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Cutter</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->cutter; ?>" type="number" step='0.001' name='cutter' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Winder 1</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->winder_1; ?>" type="number" step='0.001' name='winder_1' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Winder 2</label>
            <?php
            foreach ($tb_transaction_setting as $ts) { ?>
              <input value="<?php echo $ts->winder_2; ?>" type="number" step='0.001' name='winder_2' class="form-control" style="text-align: center">
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>

  <div class="container">
    <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Hasil Produksi</u></b></h5>
    <div class="table-responsive">
      <table class="table table-bordered border-primary">
        <thead>
          <tr>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">No</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Start</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Winder</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Type</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Panjang (m)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);" colspan="2">Tebal (mm)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Bending (mm)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Berat (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Finish</th>
          </tr>
        </thead>
        <tbody>
          <?php
          //var_dump ($tb_transaction_hasil_produksi[0]->winder); die;
          if (!empty($tb_transaction_hasil_produksi)) {
            $no = 1;
            foreach ($tb_transaction_hasil_produksi as $thp) { ?>
              <tr>
                <td style="text-align: center"><input name='no1' value="<?php echo $no; ?>" style="width: 25px;"><input type="hidden" name='id_transaction_hasil_produksi<?php echo $no; ?>' value="<?php echo $thp->id_transaction_hasil_produksi; ?>" style="width: 75px;"></td>
                <td style="text-align: center"><input value="<?php echo $thp->start; ?>" type="time" name='start<?php echo $no; ?>' style="width: 120px;  text-align: center"></td>
                <td style="text-align: center">
                  <input value="<?php echo $thp->winder; ?>" type="number" name='winder<?php echo $no; ?>' style="width: 50px;  text-align: center">
                <td style="text-align: center"><select value="<?php echo $thp->type; ?>" name='type<?php echo $no; ?>' class="form-select form-select-sm" aria-label="Default select example">
                    <option <?php echo ($thp->type == 'WIST POS') ? 'selected' : ''; ?> style="color:blue">WIST POS</option>
                    <option <?php echo ($thp->type == 'WIST NEG') ? 'selected' : ''; ?> style="color:green">WIST NEG</option>
                  </select></td>
                <td style="text-align: center"><input value="<?php echo $thp->panjang_m; ?>" name='panjang_m<?php echo $no; ?>' type="number" style="width: 80px; text-align: center"></td>
                <td style="text-align: center"><input value="<?php echo $thp->tebal_R1_mm; ?>" step='0.001' name='tebal_R1_mm<?php echo $no; ?>' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' value="<?php echo $thp->tebal_L1_mm; ?>" name='tebal_L1_mm<?php echo $no; ?>' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
                <td style="text-align: center"><input value="<?php echo $thp->tebal_R2_mm; ?>" step='0.001' name='tebal_R2_mm<?php echo $no; ?>' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' value="<?php echo $thp->tebal_L2_mm; ?>" name='tebal_L2_mm<?php echo $no; ?>' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
                <td style="text-align: center"><input value="<?php echo $thp->bending; ?>" name='bending<?php echo $no; ?>' type="number" step='0.001' style="width: 80px; text-align: center"></td>
                <td style="text-align: center"><input value="<?php echo $thp->berat_kg; ?>" step='0.01' name='berat_kg<?php echo $no; ?>' type="number" style="width: 120px; text-align: center"></td>
                <td style="text-align: center"><input value="<?php echo $thp->finish; ?>" type="time" name='finish<?php echo $no; ?>' style="width: 120px;  text-align: center"></td>
              </tr>
            <?php $no++;
            }
          } else { ?>

            <tr>
              <td style="text-align: center"><input name='no2' value="2" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder2' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type2' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m2' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm2' type="number" placeholder="R" style="width: 100px; text-align: center"><input step='0.001' name='tebal_L1_mm2' type="number" placeholder="L" style="width: 100px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm2' type="number" placeholder="R" style="width: 100px; text-align: center"><input step='0.001' name='tebal_L2_mm2' type="number" placeholder="L" style="width: 100px; text-align: center"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg2' type="number" style="width: 120px;"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no3' value="3" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder3' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type3' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m3' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm3' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L1_mm3' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm3' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L2_mm3' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg3' type="number" style="width: 120px;"></td>
            </tr>
            <tr>
              <td style="text-align: center"><input name='no4' value="4" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder4' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type4' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m4' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm4' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L1_mm4' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm4' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L2_mm4' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg4' type="number" style="width: 120px;"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no5' value="5" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder5' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type5' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m5' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm5' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L1_mm5' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm5' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L2_mm5' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg5' type="number" style="width: 120px;"></td>
            </tr>
            <tr>
              <td style="text-align: center"><input name='no6' value="6" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder6' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type6' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m6' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm6' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L1_mm6' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm6' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L2_mm6' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg6' type="number" style="width: 120px;"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no7' value="7" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder7' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type7' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m7' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm7' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L1_mm7' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm7' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L2_mm7' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg7' type="number" style="width: 120px;"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no8' value="8" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder8' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type8' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m8' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm8' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L1_mm8' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm8' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L2_mm8' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg8' type="number" style="width: 120px;"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no9' value="9" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder9' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type9' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m9' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm9' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L1_mm9' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm9' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L2_mm9' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg9' type="number" style="width: 120px;"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no10' value="10" style="width: 25px;"></td>
              <td style="text-align: center"><input name='winder10' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type10' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m10' type="number" style="width: 120px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm10' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L1_mm10' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm10' type="number" placeholder="R" style="width: 100px;"><input step='0.001' name='tebal_L2_mm10' type="number" placeholder="L" style="width: 100px;"></td>
              <td style="text-align: center"><input step='0.01' name='berat_kg10' type="number" style="width: 120px;"></td>
            </tr>
          <?php } ?>

        </tbody>
      </table>
    </div>
  </div>

  <div class="container">
    <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Input Raw Material</u></b></h5>
    <div class="table-responsive">
      <table class="table table-bordered border-primary">
        <thead>
          <tr>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">No</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">CH4 (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">CH5 (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">MA Ca-26 (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">MA Sn(kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Scrap (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">MLR (kg)</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($tb_transaction_input_material)) {
            $no = 1;
            foreach ($tb_transaction_input_material as $tim) { ?>
              <tr>
                <td style="text-align: center"><input name='no1' value="<?php echo $no; ?>" style="width: 25px;"><input type="hidden" name='id_transaction_input_material<?php echo $no; ?>' value="<?php echo $tim->id_transaction_input_material; ?>" style="width: 75px;"></td>
                <td style="text-align: center"><input value="<?php echo $tim->ch4_kg; ?>" type="number" name='ch4_kg<?php echo $no; ?>' style="width: 150px; text-align: center">
                <td style="text-align: center"><input value="<?php echo $tim->ch5_kg; ?>" name='ch5_kg<?php echo $no; ?>' type="number" style="width: 150px; text-align: center">
                <td style="text-align: center"><input value="<?php echo $tim->ma_ca_26_kg; ?>" name='ma_ca_26_kg<?php echo $no; ?>' type="number" style="width: 150px; text-align: center">
                <td style="text-align: center"><input value="<?php echo $tim->ma_sn_kg; ?>" name='ma_sn_kg<?php echo $no; ?>' type="number" style="width: 150px; text-align: center">
                <td style="text-align: center"><input value="<?php echo $tim->scrap_kg; ?>" name='scrap_kg<?php echo $no; ?>' type="number" style="width: 150px; text-align: center">
                <td style="text-align: center"><input value="<?php echo $tim->mlr_kg; ?>" name='mlr_kg<?php echo $no; ?>' type="number" style="width: 150px; text-align: center">
              </tr>
            <?php $no++;
            }
          } else { ?>
            <tr>
              <td style="text-align: center"><input name='no1' value="<?php echo $no; ?>" style="width: 25px;"></td>
              <td style="text-align: center"><input name='ch4_kg2' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ch5_kg2' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ma_ca_26_kg2' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ma_sn_kg2' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='scrap_kg2' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='mlr_kg2' type="number" style="width: 150px;">
            </tr>

            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">3</th>
              <td style="text-align: center"><input name='ch4_kg3' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ch5_kg3' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ma_ca_26_kg3' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ma_sn_kg3' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='scrap_kg3' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='mlr_kg3' type="number" style="width: 150px;">
            </tr>

            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">4</th>
              <td style="text-align: center"><input name='ch4_kg4' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ch5_kg4' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ma_ca_26_kg4' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ma_sn_kg4' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='scrap_kg4' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='mlr_kg4' type="number" style="width: 150px;">
            </tr>

            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">5</th>
              <td style="text-align: center"><input name='ch4_kg5' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ch5_kg5' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ma_ca_26_kg5' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='ma_sn_kg5' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='scrap_kg5' type="number" style="width: 150px;">
              <td style="text-align: center"><input name='mlr_kg5' type="number" style="width: 150px;">
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="container">
    <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Dross</u></b></h5>
    <div class="table-responsive">
      <table class="table table-bordered border-primary">
        <thead>
          <tr>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Pot Positif (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Pot Negatif (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Pot Scrap Posisitf (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Pot Scrap Negatif (kg)</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <?php
            foreach ($tb_transaction_dross as $td) { ?>
              <input value="<?php echo $td->id_transaction_dross; ?>" type="hidden" type="number;" name='id_transaction_dross' style="width: 150px;text-align: center">
              <td style="text-align: center"><input value="<?php echo $td->pot_pos_kg; ?>" type="number" name='pot_pos_kg' style="width: 150px;text-align: center"></td>
              <td style="text-align: center"><input value="<?php echo $td->pot_neg_kg; ?>" type="number" name='pot_neg_kg' style="width: 150px;text-align: center"></td>
              <td style="text-align: center"><input value="<?php echo $td->pot_sc_pos_kg; ?>" type="number" name='pot_sc_pos_kg' style="width: 150px;text-align: center"></td>
              <td style="text-align: center"><input value="<?php echo $td->pot_sc_neg_kg; ?>" type="number" name='pot_sc_neg_kg' style="width: 150px;text-align: center"></td>
            <?php } ?>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="container">
    <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Level Melting Pot</u></b></h5>

    <div class="table-responsive">
      <table class="table table-bordered border-primary">
        <thead>
          <tr>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">No</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Melting Pot</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Awal Shift (kg)</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Akhir Shift (kg)</th>

          </tr>
        </thead>
        <tbody>

          <?php
          foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
            <input value="<?php echo $tlm->id_level_meltingpot; ?>" type="hidden" name='id_level_meltingpot' style="width: 250px; text-align: center ">
          <?php } ?>
          <tr>
            <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">1</th>
            <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">Positif (CH5)</th>
            <td style="text-align: center;">
              <?php
              foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
                <input value="<?php echo $tlm->positif_aw_kg; ?>" type="number" name='positif_aw_kg' style="width: 250px; text-align: center ">
              <?php } ?>
            </td>
            <td style="text-align: center"><?php
                                            foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
                <input value="<?php echo $tlm->positif_ak_kg; ?>" type="number" name='positif_ak_kg' style="width: 250px; text-align: center ">
              <?php } ?>
            </td>
          </tr><samp></samp>

          <tr>
            <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">2</th>
            <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">Negatif (CH4)</th>
            <td style="text-align: center"><?php
                                            foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
                <input value="<?php echo $tlm->negatif_aw_kg; ?>" type="number" name='negatif_aw_kg' style="width: 250px; text-align: center ">
              <?php } ?>
            </td>
            <td style="text-align: center"><?php
                                            foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
                <input value="<?php echo $tlm->negatif_ak_kg; ?>" type="number" name='negatif_ak_kg' style="width: 250px; text-align: center ">
              <?php } ?>
            </td>
          </tr>

          <tr>
            <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">3</th>
            <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">Scrap positif</th>
            <td style="text-align: center"><?php
                                            foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
                <input value="<?php echo $tlm->scrap_positif_aw_kg; ?>" type="number" name='scrap_positif_aw_kg' style="width: 250px; text-align: center ">
              <?php } ?>
            </td>
            <td style="text-align: center"><?php
                                            foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
                <input value="<?php echo $tlm->scrap_positif_ak_kg; ?>" type="number" name='scrap_positif_ak_kg' style="width: 250px; text-align: center ">
              <?php } ?>
            </td>
          </tr>

          <tr>
            <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">4</th>
            <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">Scrap negatif</th>
            <td style="text-align: center"><?php
                                            foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
                <input value="<?php echo $tlm->scrap_negatif_aw_kg; ?>" type="number" name='scrap_negatif_aw_kg' style="width: 250px; text-align: center ">
              <?php } ?>
            </td>
            <td style="text-align: center"><?php
                                            foreach ($tb_transaction_level_meltingpot as $tlm) { ?>
                <input value="<?php echo $tlm->scrap_negatif_ak_kg; ?>" type="number" name='scrap_negatif_ak_kg' style="width: 250px; text-align: center ">
              <?php } ?>
            </td>
          </tr>

        </tbody>
      </table>
    </div>
  </div>

  <div class="container">
    <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Downtime Non Andon</u></b></h5>
    <div class="table-responsive">

      <table class="table table-bordered border-primary">
        <thead>
          <tr>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">No</th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Nama </th>
            <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Waktu (menit)</th>
          </tr>
        </thead>

        <tbody>
          <?php
          if (!empty($tb_transaction_downtime)) {
            $no = 1;
            foreach ($tb_transaction_downtime as $ttd) { ?>
              <tr>
                <td style="text-align: center"><input name='no<?php echo $no; ?>' value="<?php echo $no; ?>" style="width: 25px; text-align: center">
                  <input value="<?php echo $ttd->id_downtime; ?>" type="hidden" name='id_downtime<?php echo $no; ?>' style="width: 75px; text-align: center ">
                </td>
                <td style="text-align: center">
                  <select class="form-select" name='nama_downtime<?php echo $no; ?>' aria-label="Default select example">
                    <option selected>--Pilih nama downtime non andon--</option>

                    <?php
                    foreach ($data_downtime as $d_downtime) {
                      $selected = "";

                      if ($ttd->nama_downtime == $d_downtime['nama_downtime']) {
                        $selected = "selected";
                      } ?>
                      <option <?php echo $selected; ?>><?php echo $d_downtime['nama_downtime']; ?> </option>
                    <?php } ?>

                  </select>
                </td>
                <td style="text-align: center"><input value="<?php echo $ttd->waktu_m; ?>" type="number" name="waktu<?php echo $no; ?>" style="width: 200px; text-align: center"></td>
              </tr>
            <?php $no++;
            }
          } else { ?>

            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">2</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime2' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu2" style="width: 200px;"></td>
            </tr>

            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">3</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime3' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu3" style="width: 200px;"></td>
            </tr>

            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">4</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime4' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu4" style="width: 200px;"></td>
            </tr>

            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">5</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime5' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu5" style="width: 200px;"></td>
            </tr>
        </tbody>
      <?php } ?>

      </table>
    </div>
  </div>

  <div class="container mb-3" style="text-align: center;">
    <button type="submit" class="btn btn-success" style="width: 200px;">UPDATE</button>
  </div>
  </div>
</form>
</body>

</html>