</div>

<!-- Table Editable plugin-->
<script src="<?= base_url(); ?>assets/libs/jquery-tabledit/jquery.tabledit.min.js"></script>

<!-- Table editable init-->
<script src="<?= base_url(); ?>assets/js/pages/tabledit.init.js"></script>

<!-- Vendor js -->
<script src="<?= base_url('assets/js/vendor.min.js'); ?>"></script>

<!-- App js -->
<script src="<?= base_url('assets/js/app.min.js'); ?>"></script>

</body>

</html>
