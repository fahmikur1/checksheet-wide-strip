<div class="container-fluid mt-5">

    <!-- start page title -->
    <div class="row">
        <div class="col-10">
            <div class="page-title-box">
                <h4 class="page-title">Profile</h4>
            </div>
        </div>
        <div class="col-2">
          <a href="http://localhost/lhp_wide_strip/home" class="btn btn-sm btn-link d-flex justify-content-end fs-4"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="card text-center">
                <div class="card-body">
                    <img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">

                    <h4 class="mb-0"><?= $nama; ?></h4>
                    <div class="text-start mt-3">
                        <h4 class="font-8 text-uppercase">About Me :</h4>
                        <p class="text-muted font-8 mb-3">
                            Hi I'm <?= $nama; ?>
                        </p>
                        <p class="text-muted mb-2 font-8"><strong>Full Name :</strong> <span class="ms-2"><?= $nama; ?></span></p>

                        <p class="text-muted mb-2 font-8"><strong>Username :</strong><span class="ms-2"><?= $username; ?></span></p>
                    </div>
                </div>
            </div> <!-- end card -->
        </div> <!-- end col-->
        <div class="col-3"></div>
    </div>
    <!-- end row-->

</div> <!-- container -->