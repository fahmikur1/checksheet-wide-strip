
  <form class="alert mt-5" method="post" action="<?php echo base_url('lhp_wide_strip/tambah_aksi'); ?>">
    <div class="container">
      <table class="table table-bordered border-muted">
        <tbody>
          <tr style="border:none;">
            <td style="border:none;"><img src="<?= base_url('assets/images/iPTCBI.png') ?>" alt="logo CBI" style="width: 200px;"></td>
            <td style="border:none;">
              <h1 class="alert " style="border: none; text-align: left;"><b> CHECKSHEET WIDE STRIP</b></h1>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    </div>



    <div class="container">
      <div class="row">
        <div class="col-md-2">
          <div class="form-group">
            <label>Tanggal</label>
            <input type="date" name="tanggal" class="form-control">
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Shift</label>
            <input type="number" name="shift" class="form-control">
          </div>
        </div>
        <div class="col-md-6"></div>
        <div class="col-md-2">
          <a href="http://localhost/lhp_wide_strip/home" class="btn btn-sm btn-link d-flex justify-content-end fs-4"><i class="mdi mdi-keyboard-backspace"></i> Back</a>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Operator Roll Mill</label>
            <select class="form-select" name="operator_rollmill" aria-label="Default select example">
              <option selected>--Pilih nama operator--</option>
              <?php
              foreach ($data_operator as $d_operator) { ?>
                <option><?php echo $d_operator['operator']; ?> </option>
              <?php } ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>Operator Caster</label>
            <select class="form-select" name="operator_caster" aria-label="Default select example">
              <option selected>--Pilih nama operator--</option>
              <?php
              foreach ($data_operator as $d_operator) { ?>
                <option><?php echo $d_operator['operator']; ?> </option>
              <?php } ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>Operator Feeder</label>
            <select class="form-select" name="operator_support" aria-label="Default select example">
              <option selected>--Pilih nama operator--</option>
              <?php
              foreach ($data_operator as $d_operator) { ?>
                <option><?php echo $d_operator['operator']; ?> </option>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Setting Parameter Proses</u></b></h5>

      <div class="container">
        <h6 class="alert fs-4 text-center " style="border: none"><b>Temperatur Melting Pot</b></h6>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label>Positif</label>
              <input type="number" name='pot_positif' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Negatif</label>
              <input type="number" name='pot_negatif' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Scrap Positif</label>
              <input type="number" name='pot_scrap_positif' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Scrap Negatif</label>
              <input type="number" name='pot_scrap_negatif' class="form-control" style="text-align: center">
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <h6 class="alert fs-4 text-center " style="border: none"><b>Temperatur Feedline</b></h6>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label>Pot Positif ke Caster</label>
              <input type="number" name='pot_positif_ke_caster_1' class="form-control" style="text-align: center">
              <input type="number" name='pot_positif_ke_caster_2' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Pot Negatif ke Caster</label>
              <input type="number" name='pot_negatif_ke_caster_1' class="form-control" style="text-align: center">
              <input type="number" name='pot_negatif_ke_caster_2' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Pot Scrap Positif ke Pot Positif</label>
              <input type="number" name='pot_scrap_positif_ke_pot_positif_1' class="form-control" style="text-align: center">
              <input type="number" name='pot_scrap_positif_ke_pot_positif_2' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Pot Scrap Negatif ke Pot Negatif </label>
              <input type="number" name='pot_scrap_negatif_ke_pot_negatif_1' class="form-control" style="text-align: center">
              <input type="number" name='pot_scrap_negatif_ke_pot_negatif_2' class="form-control" style="text-align: center">
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <h6 class="alert fs-4 text-center " style="border: none"><b>Setting Rollmill</b></h6>

        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              <label>Manual Speed (m/min)</label>
              <input type="number" step='0.01' name='manual_speed' class="form-control" style="text-align: center">
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <h6 class="alert fs-4 text-left " style="border: none">Ratio Speed (m/min)</h6>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <div class="form-group">
              <label>Forming</label>
              <input type="number" step='0.001' name='forming' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Feeder</label>
              <input type="number" step='0.001' name='feeder' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Roll 1</label>
              <input type="number" step='0.001' name='roll_1' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Roll 2</label>
              <input type="number" step='0.001' name='roll_2' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Roll 3</label>
              <input type="number" step='0.001' name='roll_3' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Roll 4</label>
              <input type="number" step='0.001' name='roll_4' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Roll 5</label>
              <input type="number" step='0.001' name='roll_5' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Roll 6</label>
              <input type="number" step='0.001' name='roll_6' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Roll 7</label>
              <input type="number" step='0.001' name='roll_7' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Cutter</label>
              <input type="number" step='0.001' name='cutter' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Winder 1</label>
              <input type="number" step='0.001' name='winder_1' class="form-control" style="text-align: center">
            </div>
          </div>

          <div class="col-md-2">
            <div class="form-group">
              <label>Winder 2</label>
              <input type="number" step='0.001' name='winder_2' class="form-control" style="text-align: center">
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>

    <div class="container">
      <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Hasil Produksi</u></b></h5>
      <div class="table-responsive">
        <table class="table table-bordered border-primary">
          <thead>
            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">No</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Start</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Winder</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Type</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Panjang (m)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);" colspan="2">Tebal (mm)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Bending (mm)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Berat (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Finish</th>
            </tr>
          </thead>
          <tbody>

            <tr>
              <td style="text-align: center"><input name='no1' value="1" style="width: 25px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='start1' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder1' type="number" style="width: 50px;" style="text-align: center"></td>
              <td style="text-align: center"><select name='type1' class="form-select form-select-sm" aria-label="Default select example" style="text-align: center">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m1' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm1' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm1' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm1' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm1' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending1' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg1' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish1' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no2' value="2" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start2' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder2' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type2' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m2' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm2' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm2' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm2' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm2' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending2' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg2' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish2' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no3' value="3" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start3' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder3' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type3' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m3' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm3' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm3' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm3' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm3' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending3' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg3' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish3' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>
            <tr>
              <td style="text-align: center"><input name='no4' value="4" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start4' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder4' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type4' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m4' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm4' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm4' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm4' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm4' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending4' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg4' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish4' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no5' value="5" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start5' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder5' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type5' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m5' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm5' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm5' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm5' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm5' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending5' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg5' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish5' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>
            <tr>
              <td style="text-align: center"><input name='no6' value="6" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start6' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder6' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type6' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m6' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm6' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm6' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm6' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm6' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending6' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg6' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish6' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no7' value="7" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start7' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder7' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type7' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m7' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm7' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm7' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm7' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm7' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending7' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg7' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish7' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no8' value="8" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start8' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder8' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type8' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m8' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm8' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm8' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm8' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm8' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending8' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg8' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish8' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no9' value="9" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start9' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder9' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type9' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m9' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm9' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm9' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm9' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm9' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending9' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg9' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish9' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>

            <tr>
              <td style="text-align: center"><input name='no10' value="10" style="width: 25px;"></td>
              <td style="text-align: center"><input name='start10' type="time" style="width: 80px;" style="text-align: center"></td>
              <td style="text-align: center"><input name='winder10' type="number" style="width: 50px;"></td>
              <td style="text-align: center"><select name='type10' class="form-select form-select-sm" aria-label="Default select example">
                  <option selected style="color:blue">WIST POS</option>
                  <option selected style="color:green">WIST NEG</option>
              </td>
              <td style="text-align: center"><input name='panjang_m10' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R1_mm10' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L1_mm10' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input step='0.001' name='tebal_R2_mm10' type="number" placeholder="R" style="width: 80px; text-align: center"><input step='0.001' name='tebal_L2_mm10' type="number" placeholder="L" style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='bending10' type="number" step='0.001' style="width: 80px; text-align: center"></td>
              <td style="text-align: center"><input name='berat_kg10' type="number" style="width: 80px;"></td>
              <td style="text-align: center"><input name='finish10' type="time" style="width: 80px;" style="text-align: center"></td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>

    <div class="container">
      <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Input Raw Material</u></b></h5>
      <div class="table-responsive">
        <table class="table table-bordered border-primary">
          <thead>
            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">No</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">CH4 (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">CH5 (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">MA Ca-26 (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">MA Sn(kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Scrap (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">MLR (kg)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">1</th>
              <td style="text-align: center"><input name='ch4_kg1' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ch5_kg1' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_ca_26_kg1' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_sn_kg1' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='scrap_kg1' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='mlr_kg1' type="number" style="width: 150px; text-align: center">
            </tr>

            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">2</th>
              <td style="text-align: center"><input name='ch4_kg2' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ch5_kg2' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_ca_26_kg2' type="number" style="width: 150px;text-align: center">
              <td style="text-align: center"><input name='ma_sn_kg2' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='scrap_kg2' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='mlr_kg2' type="number" style="width: 150px; text-align: center">
            </tr>

            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">3</th>
              <td style="text-align: center"><input name='ch4_kg3' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ch5_kg3' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_ca_26_kg3' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_sn_kg3' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='scrap_kg3' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='mlr_kg3' type="number" style="width: 150px; text-align: center">
            </tr>

            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">4</th>
              <td style="text-align: center"><input name='ch4_kg4' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ch5_kg4' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_ca_26_kg4' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_sn_kg4' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='scrap_kg4' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='mlr_kg4' type="number" style="width: 150px; text-align: center">
            </tr>

            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">5</th>
              <td style="text-align: center"><input name='ch4_kg5' type="number" style="width: 150px;text-align: center">
              <td style="text-align: center"><input name='ch5_kg5' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_ca_26_kg5' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='ma_sn_kg5' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='scrap_kg5' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='mlr_kg5' type="number" style="width: 150px; text-align: center">
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="container">
      <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Dross</u></b></h5>
      <div class="table-responsive">
        <table class="table table-bordered border-primary">
          <thead>
            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Pot Positif (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Pot Negatif (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Pot Scrap Posisitf (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Pot Scrap Negatif (kg)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="text-align: center"><input name='pot_pos_kg' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='pot_neg_kg' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='pot_sc_pos_kg' type="number" style="width: 150px; text-align: center">
              <td style="text-align: center"><input name='pot_sc_neg_kg' type="number" style="width: 150px; text-align: center">
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="container">
      <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Level Melting Pot</u></b></h5>

      <div class="table-responsive">
        <table class="table table-bordered border-primary">
          <thead>
            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">No</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Melting Pot</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Awal Shift (kg)</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Akhir Shift (kg)</th>

            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">1</th>
              <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">Positif (CH5)</th>
              <td style="text-align: center;"><input name='positif_aw_kg' type="number" style="width: 250px;text-align: center "></td>
              <td style="text-align: center"><input name='positif_ak_kg' type="number" style="width: 250px; text-align: center"></td>
            </tr><samp></samp>

            <tr>
              <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">2</th>
              <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">Negatif (CH4)</th>
              <td style="text-align: center"><input name='negatif_aw_kg' type="number" style="width: 250px;text-align: center "></td>
              <td style="text-align: center"><input name='negatif_ak_kg' type="number" style="width: 250px; text-align: center"></td>
            </tr>

            <tr>
              <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">3</th>
              <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">Scrap positif</th>
              <td style="text-align: center"><input name='scrap_positif_aw_kg' type="number" style="width: 250px; text-align: center"></td>
              <td style="text-align: center"><input name='scrap_positif_ak_kg' type="number" style="width: 250px; text-align: center"></td>
            </tr>

            <tr>
              <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">4</th>
              <th scope="row" style="text-align: center; color:rgb(66, 66, 193);">Scrap negatif</th>
              <td style="text-align: center"><input name='scrap_negatif_aw_kg' type="number" style="width: 250px; text-align: center"></td>
              <td style="text-align: center"><input name='scrap_negatif_ak_kg' type="number" style="width: 250px; text-align: center"></td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>

    <div class="container">
      <h5 class="alert fs-4 text-left  " style="border: none"><b><u>Downtime Non Andon</u></b></h5>
      <div class="table-responsive">

        <table class="table table-bordered border-primary">
          <thead>
            <tr>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">No</th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Nama </th>
              <th scope="col" style="text-align: center; color:rgb(66, 66, 193);">Waktu (menit)</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">1</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime1' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu1" style="width: 200px; text-align: center"></td>
            </tr>

            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">2</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime2' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu2" style="width: 200px;  text-align: center"></td>
            </tr>

            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">3</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime3' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu3" style="width: 200px; text-align: center"></td>
            </tr>

            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">4</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime4' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu4" style="width: 200px; text-align: center"></td>
            </tr>

            <tr>
              <th style="text-align: center; color:rgb(66, 66, 193);">5</th>
              <td style="text-align: center">
                <select class="form-select" name='nama_downtime5' aria-label="Default select example">
                  <option selected>--Pilih nama downtime non andon--</option>
                  <?php
                  foreach ($data_downtime as $d_downtime) { ?>
                    <option><?php echo $d_downtime['nama_downtime']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: center"><input type="number" name="waktu5" style="width: 200px; text-align: center"></td>
            </tr>
          </tbody>

        </table>
      </div>
    </div>

    <div class="container mb-3" style="text-align: center;">
      <input type="submit" class="btn btn-success" value="SUMBIT" style="width: 200px;">
    </div>
    </div>
  </form>