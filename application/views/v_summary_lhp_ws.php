<div class="container mt-5">
  <table class="table table-bordered border-white">
    <tbody>
      <tr>
        <td><img src="<?= base_url('assets/images/iPTCBI.png') ?>" alt="logo CBI" style="width: 200px;" style="border: none;"></td>
        <td>
          <h1 class="alert " style=" text-align: left;"><b>DATA CHECKSHEET WIDE STRIP</b></h1>
        </td>
      </tr>
    </tbody>
  </table>
</div>

<div class="container table-responsive">
  <a target="_blank" href='<?= base_url('/lhp_wide_strip') ?>' type="button" class="btn btn-success" style="margin-bottom: 7px;">Tambah Checksheet +</a>
  <table id="btn-editable" class="table table-striped table-centered mb-0 table-bordered border-secondary ">
    <thead>
      <tr>
        <th scope="col" style=" text-align: center;">Tanggal</th>
        <th scope="col" style=" text-align: center;">Shift</th>
        <th scope="col" style=" text-align: center;">Operator Rollmill</th>
        <th scope="col" style=" text-align: center;">Operator Caster</th>
        <th scope="col" style=" text-align: center;">Operator Feeder</th>
        <th scope="col" style=" text-align: center;">Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($tr_operator as $t_operator) { ?>
        <tr>
          <!-- tanggal dari database -->
          <td style=" text-align: center;"><?php echo $t_operator['tanggal']; ?></td>
          <td style=" text-align: center;"><?php echo $t_operator['shift']; ?></td>
          <td style=" text-align: center;"><?php echo $t_operator['operator_rollmill']; ?></td>
          <td style=" text-align: center;"><?php echo $t_operator['operator_caster']; ?></td>
          <td style=" text-align: center;"><?php echo $t_operator['operator_support']; ?></td>
          <td style=" text-align: center;"> <a target="_blank" href='<?= base_url(); ?>lhp_wide_strip/edit/<?php echo $t_operator['tanggal'] ?>/<?php echo $t_operator['shift'] ?>' type="button" class="btn btn-primary">Detail</a>
            <a target="_blank" href='<?= base_url(); ?>lhp_wide_strip/delete/<?php echo $t_operator['tanggal'] ?>/<?php echo $t_operator['shift'] ?>' type="button" class="btn btn-danger">Delete</a>
          </td>
        <?php } ?>
        </tr>
    </tbody>
  </table>
</div>