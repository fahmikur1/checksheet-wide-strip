-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2020 at 02:04 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siswa_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`username`, `password`, `nama`) VALUES
('ADMIN', '$2y$10$rH0QFSZC8peQZj1trO1S8u4RUWX8X8RKcA3YnIPWf3131qyWLVSTe', 'ADMIN'),
('AFA', '$2y$10$626gdHEldoAekrTfW6QKIucnfV3DOM36QTiaFjtvON.uv62qGMLnu', 'AHMAD FARHAN'),
('DAR', '$2y$10$hyNrmOBgwQUgWj.PqnS7K.x/2Rfc/d.RSTCg8rhs4zYu3UlHGiAb6', 'DEDI ARIYANTO'),
('FAT', '$2y$10$ix2iVxaHWtj99ZnrPpsY3.fTHZG0k6IHhcexWEP5Mo/iVKbBIVwU2', 'FATKHUROHMAN'),
('ICS', '$2y$10$lcEVL5zgz98Twlve.M6mhOTtkop.aJQSCaMXhBLvff6wM3sbAHyWS', 'INDRA CAHYO SAPUTRO'),
('MFH', '$2y$10$6DeIF1hD94Ufc5dxmvk9E.LSvrHxqHxHfdSEvPa/K49RaqlpltcV2', 'MAOLANA FAJAR HARYADI'),
('MHI', '$2y$10$Q/L5Z3EGk18WxTfyp3pDcenVOZDJCgYFJaOxpcYf8H0YWO8opIGIS', 'MARTIN HIDAYATULLOH'),
('MSU', '$2y$10$4T/S4Cc4Xkd.8WdmaMZare4DkEBTXHRaYOYkSKoOBTBk/BOmoEz.C', 'MIFTAH SURURI'),
('MYO', '$2y$10$hiFwS2T7r9v1ylw7949mjeo8aX37PaAN5WNRfrUPHgIQaezGY1n6e', 'MOHAMAD YOPI'),
('PHS', '$2y$10$r5TYBs8gJy2ekPohTtEqAOIPhIQ23azHW42wGv5dJSJ1M1LCJTd/q', 'POLIN HASINTONGAN SIMANULLANG')

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
