-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2023 at 09:20 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lhp_wide_strip`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_downtime`
--

CREATE TABLE `tb_data_downtime` (
  `id_data_downtime` int(15) NOT NULL,
  `nama_downtime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_data_downtime`
--

INSERT INTO `tb_data_downtime` (`id_data_downtime`, `nama_downtime`) VALUES
(1, 'Memanaskan Caster'),
(2, 'Memanaskan Feedline'),
(3, 'Memindahkan Flexible Pipe'),
(4, 'Memanaskan Flexible Pipe'),
(5, 'Oleh grease ke Caster'),
(6, 'Cek komposisi material'),
(7, 'Adjust komposisi'),
(8, 'Setting roll mill'),
(9, 'Memanaskankan pot positif'),
(10, 'Memanaskankan pot negatif'),
(11, 'Memanaskankan pot scrap positif'),
(12, 'Memanaskankan pot scrap negatif'),
(13, 'P5M'),
(14, '5S'),
(15, 'Timah Habis'),
(16, 'APD Habis'),
(17, 'Winder Habis'),
(18, 'Gangguan Listrik'),
(19, 'Flexible pipe mampet'),
(20, 'Flexible pipe bocor'),
(21, 'Ganti flexible pipe bocor');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_operator`
--

CREATE TABLE `tb_data_operator` (
  `id_operator` int(6) NOT NULL,
  `operator` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_data_operator`
--

INSERT INTO `tb_data_operator` (`id_operator`, `operator`) VALUES
(1, 'Indra Cahyo Saputro'),
(2, 'Maolana Fajar Haryadi'),
(3, 'Dedi Ariyanto'),
(4, 'Martin Hidayatulloh'),
(5, 'M. Yopi'),
(6, 'Akhmad Farhan'),
(7, 'M. Sururi'),
(8, 'Imam Mujahid');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaction_downtime`
--

CREATE TABLE `tb_transaction_downtime` (
  `id_downtime` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `shift` int(1) NOT NULL,
  `nama_downtime` varchar(100) NOT NULL,
  `waktu_m` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_transaction_downtime`
--

INSERT INTO `tb_transaction_downtime` (`id_downtime`, `tanggal`, `shift`, `nama_downtime`, `waktu_m`) VALUES
(24, '2023-03-30', 1, 'P5M', 5),
(25, '2023-03-30', 1, 'Memanaskan Feedline', 25),
(26, '2023-03-30', 1, 'Memindahkan Flexible Pipe', 15),
(29, '2023-03-30', 2, 'Oleh grease ke Caster', 5),
(30, '2023-03-30', 2, 'APD Habis', 10),
(31, '2023-03-30', 2, 'Flexible pipe mampet', 15),
(32, '2023-03-30', 2, '--Pilih nama downtime non andon--', 0),
(33, '2023-03-30', 2, '--Pilih nama downtime non andon--', 0),
(44, '2023-04-03', 1, '5S', 5),
(45, '2023-04-03', 1, 'Memanaskan Caster', 10),
(46, '2023-04-03', 1, 'Memindahkan Flexible Pipe', 20),
(47, '2023-04-03', 1, 'Setting roll mill', 45),
(48, '2023-04-03', 1, 'Cek komposisi material', 45),
(89, '2023-04-05', 1, '--Pilih nama downtime non andon--', 0),
(90, '2023-04-05', 1, '--Pilih nama downtime non andon--', 0),
(91, '2023-04-05', 1, '--Pilih nama downtime non andon--', 0),
(92, '2023-04-05', 1, '--Pilih nama downtime non andon--', 0),
(94, '2023-04-06', 1, 'P5M', 10),
(95, '2023-04-06', 1, 'Memindahkan Flexible Pipe', 15),
(96, '2023-04-06', 1, 'Memanaskan Feedline', 20),
(97, '2023-04-06', 1, '5S', 12),
(98, '2023-04-06', 1, 'Setting roll mill', 12),
(109, '2023-03-17', 1, '--Pilih nama downtime non andon--', 0),
(110, '2023-03-17', 1, '--Pilih nama downtime non andon--', 0),
(111, '2023-03-17', 1, '--Pilih nama downtime non andon--', 0),
(112, '2023-03-17', 1, '--Pilih nama downtime non andon--', 0),
(113, '2023-03-17', 1, '--Pilih nama downtime non andon--', 0),
(119, '2023-04-10', 1, '--Pilih nama downtime non andon--', 0),
(120, '2023-04-10', 1, '--Pilih nama downtime non andon--', 0),
(121, '2023-04-10', 1, '--Pilih nama downtime non andon--', 0),
(122, '2023-04-10', 1, '--Pilih nama downtime non andon--', 0),
(123, '2023-04-10', 1, '--Pilih nama downtime non andon--', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaction_dross`
--

CREATE TABLE `tb_transaction_dross` (
  `id_transaction_dross` int(6) NOT NULL,
  `tanggal` date NOT NULL,
  `shift` int(11) NOT NULL,
  `pot_pos_kg` int(11) NOT NULL,
  `pot_neg_kg` int(11) NOT NULL,
  `pot_sc_pos_kg` int(11) NOT NULL,
  `pot_sc_neg_kg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_transaction_dross`
--

INSERT INTO `tb_transaction_dross` (`id_transaction_dross`, `tanggal`, `shift`, `pot_pos_kg`, `pot_neg_kg`, `pot_sc_pos_kg`, `pot_sc_neg_kg`) VALUES
(5, '2023-04-03', 1, 300, 250, 150, 400),
(8, '2023-04-06', 1, 300, 450, 100, 150),
(11, '2023-03-17', 1, 0, 0, 0, 0),
(13, '2023-04-10', 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaction_hasil_produksi`
--

CREATE TABLE `tb_transaction_hasil_produksi` (
  `id_transaction_hasil_produksi` int(15) NOT NULL,
  `no` int(6) NOT NULL,
  `tanggal` date NOT NULL,
  `shift` int(2) NOT NULL,
  `winder` int(2) NOT NULL,
  `panjang_m` int(11) DEFAULT NULL,
  `tebal_R1_mm` float NOT NULL,
  `tebal_L1_mm` float NOT NULL,
  `tebal_R2_mm` float NOT NULL,
  `tebal_L2_mm` float NOT NULL,
  `berat_kg` float NOT NULL,
  `type` varchar(10) NOT NULL,
  `start` time NOT NULL,
  `finish` time DEFAULT NULL,
  `bending` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_transaction_hasil_produksi`
--

INSERT INTO `tb_transaction_hasil_produksi` (`id_transaction_hasil_produksi`, `no`, `tanggal`, `shift`, `winder`, `panjang_m`, `tebal_R1_mm`, `tebal_L1_mm`, `tebal_R2_mm`, `tebal_L2_mm`, `berat_kg`, `type`, `start`, `finish`, `bending`) VALUES
(349, 1, '2023-03-30', 1, 1, 1200, 0.745, 0.746, 0.745, 0.745, 2300, 'WIST NEG', '08:00:00', '08:30:00', 0),
(350, 2, '2023-03-30', 1, 2, 1200, 0.746, 0.747, 0.747, 0.747, 2301, 'WIST NEG', '08:30:00', '09:00:00', 0),
(351, 3, '2023-03-30', 1, 1, 1200, 0.747, 0.748, 0.748, 0.748, 2302, 'WIST NEG', '00:00:00', '00:00:00', 0),
(352, 4, '2023-03-30', 1, 2, 1200, 0.748, 0.749, 0.749, 0.749, 2303, 'WIST NEG', '00:00:00', '00:00:00', 0),
(353, 5, '2023-03-30', 1, 1, 1200, 0.749, 0.75, 0.75, 0.75, 2304, 'WIST NEG', '00:00:00', '00:00:00', 0),
(354, 6, '2023-03-30', 1, 2, 1200, 0.75, 0.751, 0.751, 0.751, 2305, 'WIST NEG', '00:00:00', '00:00:00', 0),
(355, 7, '2023-03-30', 1, 1, 900, 1, 1, 1, 1, 2306, 'WIST POS', '00:00:00', '00:00:00', 0),
(356, 8, '2023-03-30', 1, 2, 900, 1.001, 1.001, 1.001, 1.001, 2307, 'WIST POS', '00:00:00', '00:00:00', 0),
(357, 9, '2023-03-30', 1, 1, 900, 1.002, 1.002, 1.002, 1.002, 2308, 'WIST POS', '00:00:00', '00:00:00', 0),
(360, 1, '2023-03-30', 2, 1, 900, 1.003, 1.003, 1.003, 1.003, 2298, 'WIST POS', '00:00:00', '00:00:00', 0),
(361, 2, '2023-03-30', 2, 2, 900, 1.003, 1.003, 1.003, 1.003, 2299, 'WIST POS', '00:00:00', '00:00:00', 0),
(362, 3, '2023-03-30', 2, 1, 900, 1.003, 1.003, 1.003, 1.003, 2300, 'WIST POS', '00:00:00', '00:00:00', 0),
(363, 4, '2023-03-30', 2, 2, 900, 1, 1, 1, 1, 2301, 'WIST POS', '00:00:00', '00:00:00', 0),
(364, 5, '2023-03-30', 2, 1, 900, 0.999, 0.999, 0.999, 0.999, 2302, 'WIST POS', '00:00:00', '00:00:00', 0),
(365, 6, '2023-03-30', 2, 2, 900, 0.998, 0.998, 0.998, 0.998, 2303, 'WIST POS', '00:00:00', '00:00:00', 0),
(366, 7, '2023-03-30', 2, 1, 1200, 0.76, 0.76, 0.76, 0.76, 2304, 'WIST NEG', '00:00:00', '00:00:00', 0),
(367, 8, '2023-03-30', 2, 2, 1200, 0.761, 0.761, 0.761, 0.761, 2305, 'WIST NEG', '00:00:00', '00:00:00', 0),
(368, 9, '2023-03-30', 2, 1, 1200, 0.762, 0.762, 0.762, 0.762, 2306, 'WIST NEG', '00:00:00', '00:00:00', 0),
(396, 1, '2023-04-03', 1, 1, 1200, 0.769, 0.769, 0.769, 0.769, 2250, 'WIST NEG', '00:00:00', '08:30:00', 0),
(397, 2, '2023-04-03', 1, 2, 1201, 0.77, 0.77, 0.77, 0.77, 2251, 'WIST NEG', '00:00:00', '09:00:00', 0),
(398, 3, '2023-04-03', 1, 1, 1202, 0.772, 0.772, 0.772, 0.772, 2252, 'WIST NEG', '00:00:00', '09:30:00', 0),
(399, 4, '2023-04-03', 1, 2, 1203, 0.77, 0.77, 0.77, 0.77, 2253, 'WIST NEG', '00:00:00', '10:00:00', 0),
(400, 5, '2023-04-03', 1, 1, 1204, 0.771, 0.77, 0.77, 0.77, 2254, 'WIST NEG', '00:00:00', '10:30:00', 0),
(492, 1, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(493, 2, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(494, 3, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(495, 4, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(496, 5, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(497, 6, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(498, 7, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(499, 8, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(500, 9, '2023-04-05', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(501, 1, '2023-04-08', 2, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(502, 1, '2023-04-06', 1, 1, 1100, 0.77, 0.776, 0.777, 0.775, 2345, 'WIST NEG', '07:30:00', '08:00:00', 0),
(503, 2, '2023-04-06', 1, 1, 900, 0, 0, 0, 0, 0, 'WIST POS', '08:30:00', '09:00:00', 0),
(504, 3, '2023-04-06', 1, 2, 900, 0, 0, 0, 0, 0, 'WIST POS', '09:00:00', '09:30:00', 0),
(505, 4, '2023-04-06', 1, 1, 900, 0, 0, 0, 0, 0, 'WIST POS', '09:30:00', '10:00:00', 0),
(506, 5, '2023-04-06', 1, 2, 900, 0, 0, 0, 0, 0, 'WIST POS', '10:00:00', '10:30:00', 0),
(507, 0, '2023-04-06', 1, 1, 900, 0, 0, 0, 0, 0, 'WIST POS', '10:30:00', '11:00:00', 0),
(508, 0, '2023-04-06', 1, 1, 1200, 0, 0, 0, 0, 0, 'WIST NEG', '13:00:00', '13:30:00', 0),
(509, 0, '2023-04-06', 1, 2, 1200, 0, 0, 0, 0, 0, 'WIST NEG', '13:30:00', '14:00:00', 0),
(510, 0, '2023-04-06', 1, 1, 1200, 0, 0, 0, 0, 0, 'WIST NEG', '14:00:00', '14:30:00', 0),
(511, 0, '2023-04-06', 1, 2, 1200, 0, 0, 0, 0, 0, 'WIST NEG', '14:30:00', '15:00:00', 0),
(512, 1, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST POS', '00:00:00', '00:00:00', 0),
(513, 2, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(514, 3, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(515, 4, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST POS', '00:00:00', '00:00:00', 0),
(516, 5, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(517, 0, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(518, 0, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(519, 0, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(520, 0, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(521, 0, '2023-04-14', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(522, 5, '2023-04-05', 2, 0, 900, 0.77, 0.77, 0.77, 0.77, 2345, 'WIST NEG', '00:00:00', '08:30:00', 0),
(523, 0, '2023-04-05', 2, 0, 900, 0.77, 0.77, 0.77, 0.771, 2345, 'WIST NEG', '00:00:00', '09:00:00', 0),
(524, 0, '2023-04-05', 2, 0, 900, 0.77, 0.77, 0.77, 0.771, 2345, 'WIST NEG', '00:00:00', '09:30:00', 0),
(525, 0, '2023-04-05', 2, 0, 900, 0.77, 0.771, 0.756, 0.756, 2345, 'WIST NEG', '00:00:00', '00:00:00', 0),
(526, 0, '2023-04-05', 2, 0, 1200, 1.001, 1.001, 1.001, 1.001, 2348, 'WIST POS', '00:00:00', '00:00:00', 0),
(527, 0, '2023-04-05', 2, 0, 1201, 1.001, 1.001, 1.001, 1, 2345, 'WIST POS', '00:00:00', '00:00:00', 0),
(528, 0, '2023-04-05', 2, 0, 1202, 0, 0, 0, 0, 2345, 'WIST POS', '00:00:00', '00:00:00', 0),
(529, 0, '2023-04-05', 2, 0, 1202, 0, 0, 0, 0, 2343, 'WIST POS', '00:00:00', '00:00:00', 0),
(530, 0, '2023-04-05', 2, 0, 1200, 0, 0, 0, 0, 2345, 'WIST POS', '00:00:00', '00:00:00', 0),
(531, 0, '2023-04-05', 2, 0, 1200, 0, 0, 0, 0, 2341, 'WIST POS', '00:00:00', '00:00:00', 0),
(532, 1, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(533, 2, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(534, 3, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(535, 4, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(536, 5, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(537, 6, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(538, 7, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(539, 8, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(540, 9, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(541, 10, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(552, 1, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 2.5),
(553, 2, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(554, 3, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(555, 4, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(556, 5, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(557, 0, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(558, 0, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(559, 0, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(560, 0, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0),
(561, 0, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 'WIST NEG', '00:00:00', '00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaction_input_material`
--

CREATE TABLE `tb_transaction_input_material` (
  `id_transaction_input_material` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `shift` int(2) NOT NULL,
  `ch4_kg` float NOT NULL,
  `ch5_kg` float NOT NULL,
  `ma_ca_26_kg` float NOT NULL,
  `ma_sn_kg` float NOT NULL,
  `scrap_kg` int(11) NOT NULL,
  `mlr_kg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_transaction_input_material`
--

INSERT INTO `tb_transaction_input_material` (`id_transaction_input_material`, `tanggal`, `shift`, `ch4_kg`, `ch5_kg`, `ma_ca_26_kg`, `ma_sn_kg`, `scrap_kg`, `mlr_kg`) VALUES
(76, '2023-03-30', 1, 1010, 1120, 1220, 0, 2000, 800),
(77, '2023-03-30', 1, 1011, 1121, 0, 0, 2001, 801),
(78, '2023-03-30', 1, 1012, 1122, 0, 0, 2002, 802),
(79, '0000-00-00', 0, 0, 0, 0, 0, 0, 0),
(80, '0000-00-00', 0, 0, 0, 0, 0, 0, 0),
(81, '2023-03-30', 2, 1230, 1100, 1212, 0, 2000, 780),
(82, '2023-03-30', 2, 1232, 1102, 0, 0, 2000, 782),
(83, '2023-03-30', 2, 0, 1104, 0, 0, 2000, 784),
(84, '2023-03-30', 2, 0, 1105, 0, 0, 2000, 781),
(85, '2023-03-30', 2, 0, 0, 0, 0, 0, 781),
(96, '2023-04-03', 1, 1002, 1001, 1200, 3400, 0, 800),
(97, '2023-04-03', 1, 1003, 1004, 0, 0, 0, 0),
(98, '2023-04-03', 1, 0, 0, 0, 0, 0, 0),
(141, '2023-04-05', 1, 0, 0, 0, 0, 0, 0),
(142, '2023-04-05', 1, 0, 0, 0, 0, 0, 0),
(143, '2023-04-05', 1, 0, 0, 0, 0, 0, 0),
(144, '2023-04-05', 1, 0, 0, 0, 0, 0, 0),
(146, '2023-04-06', 1, 1120, 1030, 1001, 300, 2000, 0),
(147, '2023-04-06', 1, 1201, 1002, 0, 500, 2000, 0),
(148, '2023-04-06', 1, 0, 0, 0, 0, 0, 0),
(149, '2023-04-06', 1, 0, 0, 0, 0, 0, 0),
(150, '2023-04-06', 1, 0, 0, 0, 0, 0, 0),
(161, '2023-03-17', 1, 0, 0, 0, 0, 0, 0),
(162, '2023-03-17', 1, 0, 0, 0, 0, 0, 0),
(163, '2023-03-17', 1, 0, 0, 0, 0, 0, 0),
(164, '2023-03-17', 1, 0, 0, 0, 0, 0, 0),
(165, '2023-03-17', 1, 0, 0, 0, 0, 0, 0),
(171, '2023-04-10', 1, 0, 0, 0, 0, 0, 0),
(172, '2023-04-10', 1, 0, 0, 0, 0, 0, 0),
(173, '2023-04-10', 1, 0, 0, 0, 0, 0, 0),
(174, '2023-04-10', 1, 0, 0, 0, 0, 0, 0),
(175, '2023-04-10', 1, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaction_level_meltingpot`
--

CREATE TABLE `tb_transaction_level_meltingpot` (
  `id_level_meltingpot` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `shift` int(1) NOT NULL,
  `positif_aw_kg` int(11) DEFAULT NULL,
  `positif_ak_kg` int(11) DEFAULT NULL,
  `negatif_aw_kg` int(11) DEFAULT NULL,
  `negatif_ak_kg` int(11) DEFAULT NULL,
  `scrap_positif_aw_kg` int(11) DEFAULT NULL,
  `scrap_positif_ak_kg` int(11) DEFAULT NULL,
  `scrap_negatif_aw_kg` int(11) DEFAULT NULL,
  `scrap_negatif_ak_kg` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_transaction_level_meltingpot`
--

INSERT INTO `tb_transaction_level_meltingpot` (`id_level_meltingpot`, `tanggal`, `shift`, `positif_aw_kg`, `positif_ak_kg`, `negatif_aw_kg`, `negatif_ak_kg`, `scrap_positif_aw_kg`, `scrap_positif_ak_kg`, `scrap_negatif_aw_kg`, `scrap_negatif_ak_kg`) VALUES
(11, '0000-00-00', 0, 20100, 18100, 20200, 18200, 20300, 18300, 20400, 18400),
(12, '2023-03-30', 2, 22000, 19010, 21990, 19020, 21890, 19030, 21790, 19040),
(15, '2023-04-03', 1, 21500, 19500, 22001, 19501, 22002, 19502, 20003, 19503),
(25, '2023-04-06', 1, 20200, 18000, 21500, 19200, 18300, 16000, 17500, 17000),
(28, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(30, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaction_operator`
--

CREATE TABLE `tb_transaction_operator` (
  `id_transaction_operator` int(11) NOT NULL,
  `tanggal` varchar(20) NOT NULL,
  `shift` int(1) NOT NULL,
  `operator_rollmill` varchar(50) NOT NULL,
  `operator_caster` varchar(50) NOT NULL,
  `operator_support` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_transaction_operator`
--

INSERT INTO `tb_transaction_operator` (`id_transaction_operator`, `tanggal`, `shift`, `operator_rollmill`, `operator_caster`, `operator_support`) VALUES
(57, '2023-04-03', 1, 'Indra Cahyo Saputro', 'Maolana Fajar Haryadi', 'M. Yopi'),
(81, '2023-04-06', 1, 'Dedi Ariyanto', 'Martin Hidayatulloh', 'Akhmad Farhan'),
(84, '2023-03-17', 1, 'Martin Hidayatulloh', 'Dedi Ariyanto', 'M. Yopi'),
(87, '2023-04-10', 1, 'Indra Cahyo Saputro', 'Martin Hidayatulloh', 'Dedi Ariyanto');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaction_setting`
--

CREATE TABLE `tb_transaction_setting` (
  `id_transaction_setting` int(6) NOT NULL,
  `tanggal` date NOT NULL,
  `shift` int(1) NOT NULL,
  `pot_positif` float NOT NULL,
  `pot_negatif` float NOT NULL,
  `pot_scrap_positif` float NOT NULL,
  `pot_scrap_negatif` float NOT NULL,
  `pot_positif_ke_caster_1` float NOT NULL,
  `pot_positif_ke_caster_2` float NOT NULL,
  `pot_negatif_ke_caster_1` float NOT NULL,
  `pot_negatif_ke_caster_2` float NOT NULL,
  `pot_scrap_positif_ke_pot_positif_1` float NOT NULL,
  `pot_scrap_positif_ke_pot_positif_2` float NOT NULL,
  `pot_scrap_negatif_ke_pot_negatif_1` float NOT NULL,
  `pot_scrap_negatif_ke_pot_negatif_2` float NOT NULL,
  `manual_speed` float NOT NULL,
  `forming` float NOT NULL,
  `feeder` float NOT NULL,
  `roll_1` float NOT NULL,
  `roll_2` float NOT NULL,
  `roll_3` float NOT NULL,
  `roll_4` float NOT NULL,
  `roll_5` float NOT NULL,
  `roll_6` float NOT NULL,
  `roll_7` float NOT NULL,
  `cutter` float NOT NULL,
  `winder_1` float NOT NULL,
  `winder_2` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_transaction_setting`
--

INSERT INTO `tb_transaction_setting` (`id_transaction_setting`, `tanggal`, `shift`, `pot_positif`, `pot_negatif`, `pot_scrap_positif`, `pot_scrap_negatif`, `pot_positif_ke_caster_1`, `pot_positif_ke_caster_2`, `pot_negatif_ke_caster_1`, `pot_negatif_ke_caster_2`, `pot_scrap_positif_ke_pot_positif_1`, `pot_scrap_positif_ke_pot_positif_2`, `pot_scrap_negatif_ke_pot_negatif_1`, `pot_scrap_negatif_ke_pot_negatif_2`, `manual_speed`, `forming`, `feeder`, `roll_1`, `roll_2`, `roll_3`, `roll_4`, `roll_5`, `roll_6`, `roll_7`, `cutter`, `winder_1`, `winder_2`) VALUES
(52, '2023-03-30', 1, 450, 451, 452, 453, 450, 451, 452, 453, 454, 455, 456, 457, 1.85, 0.95, 0.951, 0.952, 0.953, 0.954, 0.955, 0.956, 0.957, 0.958, 0.959, 0.96, 0.961),
(54, '2023-03-30', 2, 455, 454, 453, 452, 456, 457, 458, 459, 460, 461, 462, 463, 1.85, 0.9, 0.901, 0.902, 0.903, 0.904, 0.905, 0.906, 0.907, 0.908, 0.909, 0.9, 0.9),
(60, '2023-04-03', 1, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 1.85, 0.923, 0.924, 0.925, 0.926, 0.927, 0.928, 0.929, 0.93, 0.931, 0.932, 0.933, 0.932),
(75, '2023-04-05', 1, 450, 451, 452, 452, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(76, '2023-04-06', 1, 500, 451, 452, 452, 449, 448, 447, 446, 444, 443, 442, 441, 1.85, 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 1.01, 1.02, 1.03),
(79, '2023-03-17', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(82, '2023-04-10', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_data_downtime`
--
ALTER TABLE `tb_data_downtime`
  ADD PRIMARY KEY (`id_data_downtime`);

--
-- Indexes for table `tb_data_operator`
--
ALTER TABLE `tb_data_operator`
  ADD PRIMARY KEY (`id_operator`);

--
-- Indexes for table `tb_transaction_downtime`
--
ALTER TABLE `tb_transaction_downtime`
  ADD PRIMARY KEY (`id_downtime`);

--
-- Indexes for table `tb_transaction_dross`
--
ALTER TABLE `tb_transaction_dross`
  ADD PRIMARY KEY (`id_transaction_dross`);

--
-- Indexes for table `tb_transaction_hasil_produksi`
--
ALTER TABLE `tb_transaction_hasil_produksi`
  ADD PRIMARY KEY (`id_transaction_hasil_produksi`),
  ADD UNIQUE KEY `type` (`id_transaction_hasil_produksi`);

--
-- Indexes for table `tb_transaction_input_material`
--
ALTER TABLE `tb_transaction_input_material`
  ADD PRIMARY KEY (`id_transaction_input_material`);

--
-- Indexes for table `tb_transaction_level_meltingpot`
--
ALTER TABLE `tb_transaction_level_meltingpot`
  ADD PRIMARY KEY (`id_level_meltingpot`);

--
-- Indexes for table `tb_transaction_operator`
--
ALTER TABLE `tb_transaction_operator`
  ADD PRIMARY KEY (`id_transaction_operator`) USING BTREE;

--
-- Indexes for table `tb_transaction_setting`
--
ALTER TABLE `tb_transaction_setting`
  ADD PRIMARY KEY (`id_transaction_setting`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_data_downtime`
--
ALTER TABLE `tb_data_downtime`
  MODIFY `id_data_downtime` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tb_data_operator`
--
ALTER TABLE `tb_data_operator`
  MODIFY `id_operator` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_transaction_downtime`
--
ALTER TABLE `tb_transaction_downtime`
  MODIFY `id_downtime` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `tb_transaction_dross`
--
ALTER TABLE `tb_transaction_dross`
  MODIFY `id_transaction_dross` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tb_transaction_hasil_produksi`
--
ALTER TABLE `tb_transaction_hasil_produksi`
  MODIFY `id_transaction_hasil_produksi` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=562;

--
-- AUTO_INCREMENT for table `tb_transaction_input_material`
--
ALTER TABLE `tb_transaction_input_material`
  MODIFY `id_transaction_input_material` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `tb_transaction_level_meltingpot`
--
ALTER TABLE `tb_transaction_level_meltingpot`
  MODIFY `id_level_meltingpot` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_transaction_operator`
--
ALTER TABLE `tb_transaction_operator`
  MODIFY `id_transaction_operator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `tb_transaction_setting`
--
ALTER TABLE `tb_transaction_setting`
  MODIFY `id_transaction_setting` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
